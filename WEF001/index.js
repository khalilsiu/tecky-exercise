let sum = 1+1;
const cards = 52;
patterns = 4;

console.log("1+1="+ (sum));
console.log("Number of cards with the same pattern is " + (cards/patterns));
console.log("4 is not a prime number because the remainder of it dividing 2 is " + (4%2));

let player = "Nikola Jokic";
let pts = 21;
let reb = 12;
let ast = 11;
let stl = 2;
let blk = 0;
let missed_fg = 15-8;
let missed_ft = 4-4;
let to = 3;
let gp = 1;

let efficiency = (pts + reb + ast + stl + blk - missed_fg - missed_ft - to) / gp;

console.log(`${player}'s efficiency in Game 4 is ${efficiency}`);

