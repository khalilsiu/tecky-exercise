import React from 'react';

interface PropsType{
    children?: JSX.Element[] | JSX.Element;
    name?: string
}

export default class ShoppingList extends React.Component<PropsType,{}>{
    constructor(props: PropsType){
        super(props);
    }

    public render(){
        return (
            <div className="shopping-list">
                <h1>Shopping list for {this.props.name}</h1>
                <ul>
                    <li>Instagram</li>
                    <li>WhatsApp</li>
                    <li>Oculus</li>
                </ul>
            </div>
        )
    }
}