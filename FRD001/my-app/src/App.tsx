import React from 'react';
import logo from './logo.png'
import './App.css';

const App: React.FC = () => {
  return (
    <div>
      <h1>Simple Website</h1>
      This is a simple website made without React. Try to convert this into React enabled.
      <ol>
        <li>
          First, you need to use create-react-app
        </li>
        <li>
          Second, you need to run npm start
        </li>
      </ol>
      <div className="end">
        <img src={logo}></img>
      </div>
      <ShoppingList name="Mark"/>
    </div>
  );
}

export default App;


interface PropsType{
  children?: JSX.Element[] | JSX.Element;
  name?: string
}

function ShoppingList (props: PropsType){
  return (
      <div className="shopping-list">
        <h1>Shopping List for {props.name}</h1>
        <ul>
          <li>Instagram</li>
          <li>WhatsApp</li>
          <li>Oculus</li>
        </ul>
    </div>
)
}