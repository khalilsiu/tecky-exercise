import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

interface IWinningModalProps{
    winner: string;
    replay: ()=>void;
}

interface IWinningModalState{
    countdown: number
}

export default class WinningModal extends React.Component<IWinningModalProps, IWinningModalState>{
    private intervalId?: NodeJS.Timeout
    // we do not need this to be stored onto the state, do not need to re-render every time
    constructor(props: IWinningModalProps){
        super(props);
        this.state = {
            countdown:5
        }
    }

     public async componentDidMount(){
        const res = await fetch('https://restcountries.eu/rest/v2/name/hk');
        console.log(await res.json());
        this.intervalId = setInterval(()=>{
            if (this.state.countdown > 1){
                this.setState({
                    countdown: this.state.countdown-1
                })
            }
            else{
                this.props.replay();
            }
        },1000)
    }

    public componentWillUnmount(){
        if(this.intervalId){
            clearInterval(this.intervalId)
        }
    }

    public render(){
        return (
            <Modal isOpen={true}>
                <ModalHeader>Congratulations!</ModalHeader>
                    <ModalBody>
                        {this.props.winner} wins the game!
                    </ModalBody>
                    <ModalFooter>
                        <Button color='primary' onClick={this.props.replay}>Replay</Button>
                    </ModalFooter>
            </Modal>
        )
    }
}