import React from 'react';
import Square from './Square'
import WinningModal from './WinningModal'
import './Board.css'

interface IBoardState {
    squares: Array<string | null>;
    oIsNext: boolean
}


export default class Board extends React.Component<{}, IBoardState>{
    state = {
        squares: Array(9).fill(null),
        oIsNext: true
    }

    private renderSquares = (i: number) => {
        return <Square key={i} index={i} value={this.state.squares[i]} squareOnClick={this.handleClick.bind(this, i)} />
    }

    private handleClick = (i: number) => {
        const squares = this.state.squares.slice();
        if (!squares[i] && !this.calculateWinner()) {
            squares[i] = this.state.oIsNext ? 'O' : 'X';
            this.setState({
                squares: squares,
                oIsNext: !this.state.oIsNext
            })
        }
    }

    private calculateWinner = () => {
        const squares = this.state.squares
        
        const winCases = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let winCase of winCases){
            let [a,b,c] = winCase;
            if (squares[a] && squares[a]===squares[b] && squares[a]===squares[c]){
                return squares[a]
            }
        }
        return null;
    }

    private replay = ()=>{
        this.setState({
            squares: Array(9).fill(null),
            oIsNext: true
        })
    }

    public render() {
        const winner = this.calculateWinner();
        const status:string = winner? 
                `${winner} wins` : 
                `Next player is ${this.state.oIsNext? 'O': 'X'}` 
        let rows = [
            [0,1,2],
            [3,4,5],
            [6,7,8]
        ]
        return (
            <div>
                <div className='status'>{status}</div>
                {rows.map((row, i)=>{
                    return(
                        <div key={i} className='board-row'>
                            {row.map(col=>{
                                return (
                                    this.renderSquares(col)
                                )
                            })}
                        </div>
                    )
                })}
                {
                    winner && <WinningModal winner={winner} replay={this.replay}></WinningModal>
                }
            </div>
        )
    }

}