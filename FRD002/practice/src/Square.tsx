import React from 'react';
import './Square.css'

interface ISquareProps{
    index: number
    value: string | null
    squareOnClick: ()=> void
}


export default class Square extends React.Component <ISquareProps,{}>{

    constructor(props: ISquareProps){
        super(props);
    }

    public render(){
        return (
            <button className='square' onClick={this.props.squareOnClick}>{this.props.value}</button>
        )
    }
}