
window.onload=function(){
    let round_counter = 0;
    let boxes = document.querySelectorAll('.squares');
    let xBoard = document.querySelector("#x-board");
    let oBoard = document.querySelector('#o-board');
    let turn = document.querySelector("#turn");
    let wConditions = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [2,4,6]
    ];
    let xAttempt = [];
    let oAttempt = [];
    let gameBoard = document.querySelector("#game-board");
    // 
    

// boxes.addEventListener does not exist
    for (let box of boxes){
        box.addEventListener('click', function(event){
            console.log("this is clicked");
            const boxContent = box.querySelector('.box-cross, .box-circle');
            // should not check if innerHTML is blank, since if it has enter, it is not empty too
            if (!boxContent){
                // i can also do box.querySelector('div.circle','div.cross') to separate
                const content = box.querySelector("div")
                console.log(content);
                if (round_counter%2 === 0){
                    // can use box.innerHTML = `<div class="cross">X</div>`
                    content.classList.add("box-cross");
                    oBoard.classList.add("active");
                    xBoard.classList.remove("active");
                    turn.innerHTML = "O";
                    xAttempt.push(parseInt(box.id));
                    xAttempt.sort();
                    if(checkWin()){
                        document.querySelector('#game-board').classList.add("cross-win")
                    };
                    round_counter+=1;

                }
                else{
                    content.classList.add("box-circle");
                    xBoard.classList.add("active");
                    oBoard.classList.remove("active");
                    turn.innerHTML = "X";
                    oAttempt.push(parseInt(box.id));
                    xAttempt.sort();
                    if(checkWin()){
                        document.querySelector('#game-board').classList.add("circle-win")
                    };
                    round_counter+=1;
                }
            }

            
        })
    }

    function checkWin(){
        let currentBoard = [];
        for(let box of boxes){
            let icon = box.querySelector('.box-circle, .box-cross');
            
            // you are able to record the position of the array being pushed
            // by pushing null onto the list while doing a loop
            if (icon){
                currentBoard.push(box.querySelector("div").classList.value);
            }
            else{
                currentBoard.push(null);
            }
            

        }
        for (let wCondition of wConditions){
            // by checking if the elements in the positions are equal to each other
            // if yes, wins
            // this allows you to assign x to be the position argument
            const [x,y,z] = wCondition;
            console.log(currentBoard[x], currentBoard[y], currentBoard[z]);
            if (currentBoard[x] && 
                currentBoard[x]===currentBoard[y]&&
                currentBoard[y]===currentBoard[z]){
                    return true;
                }
        }
        
    }

    

}
