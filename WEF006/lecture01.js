
// onload runs after the page has loaded
// therefore, people put all the stuff inside the function 
// so that even the js file is put on header of the html, wont crash
// onclick, on ___ will overshadow previous events
// so better to use addEventListener 
window.onload = function(){
    console.log("window has been loaded")
}



// old style api
let collection = document.getElementsByTagName('body');

// can use for loop to loop through the element
// looks like array but cannot use map 
// though you can use [] to access individual elements
for (let elem of collection){
    elem.innerHTML = "";
}

// but this is not good because, if someone adds an element in html, error
// should assign id if possible
console.log(collection[2]);

let section2 = document.getElementById('section-2')

console.log(section2.innerHTML)
console.log(section.id)
console.log(section2.classList)

let sections = document.getElementsByClassName('sections')

// treat innerhtml as string
for(let elem of sections){
    elem.innerHTML += "Hello world"
}


// new style api
// choose the very first element that fulfills the css selector
const firstSection = document.querySelector('.section');


const allSections = document.querySelectorAll(".sections")
for(let section of allSections){
    console.log(section.querySelector('p[name="24"]'))
}

// can shorten it to access child elements, regardless of the css file
console.log(document.querySelector('.sections p[name="24"]'))


section2.addEventListener('click', function(event){
    // this function is called an event handler or Callback  
    console.log("Section2 has been clicked");
    // information of the event (e.g. the mouse coordinates)
    console.log(event);
    // console.target > shows the element being clicked
    console.log(event.target);
    // change the event target to something else
    event.target.innerHTML="o";
    // this refers to the section 2 (at which element the event is handled)
    // may not be the same as event.target (at which element the event is initiated)
    // better to console.log
    console.log(this)

    // add div and innerhtml 
    let node = document.createElement("div");
    node.innerHTML = "I am a new node";

    // gets the attribute of the element
    console.log(this.getAttribute("class"))

    // Most of the time, we change the classList of instead of the style directly
    this.classList.add('square');
    this.classList.remove('section')
    // vs this.style >> since the css styles syntax are all different, hard to use
    this.style.alignContent

})

section2.querySelector('h1')
    .addEventListener('click', function(event){
        // since h1 is an element of section2, both console log of section2 and heading1 will be triggered
        // whenever there is an Event Listener, but h1 first, then section2
        console.log("Heading 1 has been clicked");
        // to only limits to console.log h1
        event.stopPropagation();


    })


function helloOnLoad(){
    console.log("Hello onload");
}

window.addEventListener('load', helloOnLoad);
// rare case
window.removeEventListener('load', helloOnLoad, false);


// three possible location of code
// 1. window.onload
// 2. event handler
// 3. setInterval / setTimeout

// we put code within window.onload, such that even if the js file is put anywhere, will still load'
// css first, html, js