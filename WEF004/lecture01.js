


console.log(Array); // prints the constructor -> construct a new object from class

let arr = new Array();

console.log(arr);

class Student{
    //All the method and attributes are inside

    // A special method that is going to be run when the "new Student(...)" is called
    constructor(age,name, learningLevel, attendance){
        this.age = age; // this refers to the object student, JS does Bind for you automatically
        this.name = name;
        this.learningLevel = learningLevel;
        this.attendance = attendance;

    }

    learn(hours){
        // the method body of learn
        // this student learns, the learning level of this student increases
        this.learningLevel += hours * 0.1;


    }

    slack(hours){
        // the method body of slack
        // A student slacks, the learning level of him decreases
        this.learningLevel -= hours * 0.1;
    }
}

// constructor would only be called when you create a new instance
let peter = new Student(18, "peter", 3, 0.95);
console.log(peter);

// if you do not pass parameter to a new instance, JS wont give you error
let john = new Student(20,"john");

console.log(john);


// Dont need to change the code above

class CodingStudent extends Student {
    // you must call super constructor as a subclass 
    // Student is a superclass of Coding Student, JS only allows subclasses to have 1 super class
    constructor(name, age){
        // press cmd to access the super class
        super(name, age);
        this.codingLevel = 0;

        

        
    }
    // subclass is able to override superclass method by defining the same function
    learn(hours){
        console.log("learn of coding student is called");
        // or you can inherit the parent method by calling super
        super.learn(hours);
    }
}

let mary = new CodingStudent(25, "Mary");


mary.learn(10);
// go onto console and type mary instanceof Student >> true


class TeckyStudent extends CodingStudent {
    // If there is no constructor (meaning that if you do not have extra attributes to add)
    // then you do not need to add super()

    slack(hours){
        super.slack(hours);
        console.log
    }
}

let matthew = new TeckyStudent();