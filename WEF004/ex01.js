
class Game{
    constructor(player, monsters){
        this.player = player;
        this.monsters = monsters;

        
    }

    start(){
        for (let monster of this.monsters){
            let effectiveness = 1;
            console.log(`Player ${this.player.name} encounters a ${monster.type} monster`);
            if (this.player.type === "Fire" && monster.type === "Earth" || this.player.type === "Earth" && monster.type === "Water" || this.player.type === "Water" && monster.type === "Fire"){
                console.log(`${this.player.type} is effective against ${monster.type}, strength is doubled!`);
                effectiveness = 2;
            }
            else if (this.player.type === "Fire" && monster.type === "Water" || this.player.type === "Earth" && monster.type === "Fire" || this.player.type === "Water" && monster.type === "Earth"){
                console.log(`${this.player.type} is not effective against ${monster.type}, strength has cut in half!`);
                effectiveness = 0.5;
            }
            else{
                console.log(`${this.player.type} is leveled against ${monster.type}`);
            }

            while(monster.hp > 0){
                let isCritical = this.player.attack(monster, effectiveness);
                console.log(`Player ${this.player.name} attacks a monster (HP: ${monster.hp})`, isCritical? "[CRITICAL]" : "");
                // this.player.attack(monster, effectiveness);
            }

        }
    }
    
}


function createMonster(){
    // we can use array with random instead of switch
    // class can be passed in as variables
    const MONSTER_TYPES = [BossMonster, RareMonster, Monster];
    return new MONSTER_TYPES[Math.floor(Math.random()*3)];
}

class Player{
    constructor(strength, name){
        this.strength = strength;
        this.name = name;
        this.experienceLevel = 0;
        this.type = prompt("Choose your type: ");
        console.log(`Your type is ${this.type}`);

    }

    attack(monster, effectiveness){

        if (Math.random() > 0.8){
            monster.injure(this.strength*effectiveness * 2, this);
            return true;
        }
        else {
            monster.injure(this.strength*effectiveness, this);
            return false;
        }
        
        
            
            //console.log("multiplier:", multiplier);

    }



    gainExperience(exp){
        this.experienceLevel += exp;
        this.strength += exp;
        console.log(`Player ${this.name} gains ${exp} strength`);
    }
}


class Monster{
    constructor(){
        this.hp = (Math.floor(Math.random() * (3 - 1)) + 1)*100;
        this.fullHp = this.hp;
        let ELEMENT_TYPE = ["Water", "Fire", "Earth"];
        this.type = ELEMENT_TYPE[Math.floor(Math.random()*3)];
        


    }

    injure(strength){
        // Anything related to change in attributes of monster, should stay inside monster
        // should only see this.this.
        this.hp = this.hp - (strength * 0.2);
        // Max chooses whichever the larger number >> if this.hp is lower than 0 > return 0
        this.hp = Math.max(0, this.hp);
        //console.log("HP:", this.hp);

    }
}

class RareMonster extends Monster{
    injure(strength, player){
        this.hp -= strength * 0.2;
        this.hp = Math.max(0, this.hp);
        if (this.hp === 0){
            player.gainExperience(20);
        }
    }
}

class BossMonster extends Monster{
    constructor(){
        // if there is no parameter in the previous class definition, you still need to add super
        // just that you wouldnt need to specify the attributes again while being inherited
        super();
        this.resistance = 1;


    }

    injure(strength){
        if (this.hp < (this.fullHp * 0.5)){
            this.resistance = 2;
        }
        //console.log("resistance: ", this.resistance);
        this.hp -= strength / this.resistance * 0.2;
        this.hp < 0 ? this.hp = 0 : this.hp = this.hp;

    }
}


const player = new Player(100, "peter");

// We can use map on an array to create 3 instances of monsters
// we need to fill array with null, otherwise wouldnt work since undefined
const newGame = new Game(player, Array(3).fill(null).map((x)=>createMonster()));

newGame.start();