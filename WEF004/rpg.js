class Game{
    constructor(player, monster){
        this.player = player;
        this.monster = monster;
        player = new Player(100, this.player);

        function randHp(){
            return (Math.floor(Math.random() * (3 - 1) + 1)*100);
        } 

        for (let encounter = 0; encounter < 3; encounter++){
            let scenario = Math.floor(Math.random()*3);
            
            switch (scenario){
                case 0:
                    monster = new Monster(randHp());
                    console.log(`Player ${player.name} meet a Normal Monster (HP: ${monster.hp})`);
                    break;
                case 1:
                    monster = new RareMonster(randHp());
                    console.log(`Player ${player.name} meet a Rare Monster (HP: ${monster.hp})`);
                    break;
                case 2:
                    monster = new BossMonster(randHp());
                    console.log(`Player ${player.name} meet a Boss Monster (HP: ${monster.hp})`);
                    break;
            }
        
            while (monster.hp > 0){
                player.attack(monster);
            }
        }

    }

    
}



class Player{
    constructor(strength, name){
        this.strength = strength;
        this.name = name;
        this.expLevel = 0;
    }

    attack(monster){
        let multiplier = Math.floor(Math.random() * (3 - 1) + 1);
        //console.log(multiplier);
        let multiplied_strength = multiplier * this.strength;
        //console.log(multiplied_strength);
        monster.injure(multiplied_strength, this, multiplier);
        
    }

    gainExperience(exp){
        this.expLevel += exp;
        this.strength += exp;
    }
}

class Monster{
    constructor(hp){
        this.resistance = 1;
        this.original_hp = hp;
        this.hp = hp;
    }

    injure(strength, player, critical){
        this.hp -= strength * 0.2 / this.resistance;
        if (this.hp < 0)
            this.hp = 0;
        if (critical == 2){
            console.log(`Player ${player.name} attacks a monster (HP: ${this.hp}) [CRITICAL]`);
        }
        else
            console.log(`Player ${player.name} attacks a monster (HP: ${this.hp})`);

        
    }

}

class RareMonster extends Monster{
    constructor(hp){
        super(hp);

    }

    injure(strength, player, critical){
        super.injure(strength, player, critical);
        if (this.hp == 0){
            player.gainExperience(20);
            console.log(`Player ${player.name} gained 20 strength`);
        }
        

    }
}

class BossMonster extends Monster{
    constructor(hp){
        super(hp);
        this.resistance = 1;
    }

    injure(strength, player, critical){
        super.injure(strength, player, critical);
        if (this.hp < this.original_hp * 0.5){
            this.resistance = 2;
        }
        //console.log(this.resistance);
        
        
        

    }
}




let game1 = new Game("peter","mjarjar");


