

let number = 1.2342;

// produces a string
number.toFixed(2);


console.log((Number(number.toFixed(2))));

console.log(parseFloat(number.toFixed(2)));


//replace; useful for regular expression

console.log("The quick brown fox jumps over the lazy dog.".replace(/a/g,""));
console.log("The quick brown fox jumps over the lazy dog.".replace(/[oe]/g,""));

// map reduce filter, all can be done using for loop

// map
let evenNumbers = [2,4,6,8,10,12,14,16,18,20];
let squared = [];

for (let evenNumber of evenNumbers){
    squared.push(evenNumber*evenNumber);
}

console.log(squared);

// the return value is an array with value pushed
console.log(evenNumbers.map(function(evenNumber){
    return evenNumber*evenNumber;
}));

// the return value is the expression being pushed
console.log(evenNumbers.map(x=>x**2));


// filter

const numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
const multiplesOf5 = [];

for(let number of numbers){
    if (number % 5 == 0){
        multiplesOf5.push(number);
    }
}

console.log(multiplesOf5);
// the return value is the if condition in the loop
console.log(numbers.filter(x=>x%5==0));


// we usually use filter! and also map as well!
// example
let allHongKongers = [];
allHongKongers.filter(hkPeople => hkPeople.taxBenefit < 10000) // who eligible for 4000
    .map(hkPeople => hkPeople.money += 4000) // add 4000 to their acc
    .reduce(acc, value => acc+value,0); // calc total amount of 4000 given out





// reduce, accumulator doing operation with the next element

const values = [1,2]
let sum = 0;

for (let value of values){
    sum += value;
}
console.log(sum);

// there can be a initial value for accumulator
console.log(values.reduce((acc, x)=>acc*x,10));
 
