// step1: create 2 folders and npm init them: my-calculator and exercise-2 to create 2 projects
// step2: in the my-calculator's index.js, create a calculator class and export
// step3: since we want to create a calculator object in exercise-2 to run methods
// cont'd: and we want to use the library 'my-calculator' from another folder
// cont'd: we then need to create a link: sudo npm link ../my-calculator to fetch the file as library being required
// this method, my-calculator can be extensively used in other programs, publish online 

const Calculator = require('my-calculator');
const calculator = new Calculator();

console.log(calculator.add(1)); // print 1
console.log(calculator.add(5)); // print 6
console.log(calculator.divide(2)); // print 3
console.log(calculator.minus(2)); // print 1
console.log(calculator.multiply(99)); // print 99
console.log(calculator.clear()); // print 0
console.log(calculator.add(1)); // print 1
console.log(calculator.divide(0)); // throw error

// on the command line inside your project folder, you can enter the REPL by just typing node
// instead of node . , (Read-evaluated-print-loop) > you can type any value / expression to get evaluated / printed immediately
// essentially, running the code above too **need to initialize new object for use!**