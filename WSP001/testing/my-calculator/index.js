
const Calculator = require('../my-calculator');

const calculator = new Calculator();

console.log(calculator.add(1)); // print 1
console.log(calculator.add(5)); // print 6
console.log(calculator.divide(2)); // print 3
console.log(calculator.minus(2)); // print 1
console.log(calculator.multiply(99)); // print 99
console.log(calculator.clear()); // print 0
console.log(calculator.add(1)); // print 1
console.log(calculator.divide(0)); // throw error
