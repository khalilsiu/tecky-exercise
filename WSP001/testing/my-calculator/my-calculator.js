class Calculator{
    constructor(){
        this.result = 0;
    }

    add(num){
        this.result += num;
        return this.result;
    }

    minus(num){
        this.result -= num;
        return this.result;
    }

    multiply(num){
        this.result *= num;
        return this.result;
    }

    divide(num){
        this.result /= num;
        return this.result;
    }

    clear(){
        this.result = 0;
        return this.result;
    }
}

module.exports = Calculator;