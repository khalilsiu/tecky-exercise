const axios = require('axios');
const fs = require('fs');
const lib = require('./lib');
const Cls = require('./cls');
const func = require('./func');


// axios has to be installed, while fs is already installed
console.log(lib.someObject); // should print "Hello World"
console.log(lib.someFunction()); // should print "Foobar"
// if you do the above variable.obj in file, in the file, you would need to specify module.export.someFunction = someFunction
// otherwise, Cls() is being imported as a class already: module.export = Cls
console.log(new Cls()); // Cls is just a name
console.log(func.func()); // func is also just a name