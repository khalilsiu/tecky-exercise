

someObject = "Hello World"

function someFunction(){
    return "Foobar";
}

// need to think of module.exports as an object >> lib
// when lib.someFunction is called, it has to correspond to someFunction
module.exports.someFunction = someFunction;
module.exports.someObject = someObject;