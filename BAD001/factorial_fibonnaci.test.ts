import {factorial, fibonnaci} from './factorial_fibonnaci';


describe('factorial', ()=>{
    it('(1) should return 1',()=>{
        expect(factorial(1)).toBe(1);
    })

    it('(3) should return 6', ()=>{
        expect(factorial(3)).toBe(6)
    })

    it('(18) should return 6402373705728000',()=>{
        expect(factorial(18)).toBe(6402373705728000)
    })
})

describe('fibonnaci',()=>{
    it('(1) should return 1',()=>{
        expect(fibonnaci(1)).toBe(1)
    })
    it('(3) should return 3',()=>{
        expect(fibonnaci(3)).toBe(3)
    })
})