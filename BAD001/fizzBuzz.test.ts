
import {fizzBuzz} from './fizzBuzz';

describe("fizzBuzz",()=>{

    it('(1) should return [1]',()=>{
        expect(fizzBuzz(1)).toEqual([1])
    })

    it('(3) should return [1, 2, "Fizz"]',()=>{
        expect(fizzBuzz(3)).toEqual([1, 2, "Fizz"])
    })

    it('(5) should return [1, 2, "Fizz", 4, "Buzz"]',()=>{
        expect(fizzBuzz(5)).toEqual([1, 2, "Fizz", 4, "Buzz"])
    })

    it('(15) should return [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "Fizz Buzz"]',()=>{
        expect(fizzBuzz(15)).toEqual([1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "Fizz Buzz"])
    })
})