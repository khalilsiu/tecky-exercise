
let pattern = "^matchthis$";
let flags = "i"
let reConstructor = new RegExp(pattern, flags)

console.log(reConstructor.test("MATCHTHIS")); //true


let theSpecialChars =  "[({})]\\ ^$.|?*+" // \means escape, needs another \ to print out
console.log(theSpecialChars);

// let reMatchTheSpecialChars = /[({})]\\ ^$.|?*+/  // all special chars, wouldnt be able to match like this

let reMatchTheSpecialChars = /\[\(\{\}\)\]\\ \^\$\.\|\?\*\+/ // on the left of the char, true
console.log(reMatchTheSpecialChars.test(theSpecialChars))

let phoneNumber = "+46-702-11 12 13"


let isValidNumber = phoneNumber.match(/^[\+\d\-\s]+$/) // match the start to the end, any at least 1 charset that matches any of the + digit, - and space
console.log(`is ${isValidNumber?"valid":"invalid"}`);
let text = "david is writing code. david is in a video. david likes regex"

// Extraction
let extractDavid = text.match(/david/g) // global, otherwise matches the first occurrence
console.log(`${extractDavid.length} occurrances of david in '${text}'`)

// Replacement

let replaceDavid = text.replace(/david/g, "devtips")
console.log(replaceDavid)

// aaaa - aaaaaaaa ('aaaa' 'aaaa')
// a{6} - aaaaaaaa ('aaaa' aa) >> matching 6 char of a
// a{4,} - aaaaaaaa ('aaaaaaaa') >> matching at least 4 or more
// a{4,5} - aaaaaaaaa ('aaaaa' 'aaaa') 
// a{0,} - aaaaaaaa ('' 'aaaaaaaa')
// a* - aaaaaaaa ('' 'aaaaaaaa') >> shorthand of the above, 0 and above
// a{0,1} - babababa (''b'a'b'a'b'a'b'a') - empty string or 1 a
// a? - same as above
// a{1,} - aaaaaa ('aaaaaaa') - at least 1 
// a+ - same as above
// [xyz] - yyyzzzyyxx > match any of the character
// [xyz]+ - yyyyzzzyyxx > match the entire of each of those
// [0-9]+ - any integer 9832495834
// [a-z]+ osinefowie
// [a-z0-9]+ x123nwe
// [a-z0-9A-Z_]+ x123asdfSD_
// [\w] shorthand of the above
// [\d]+ shorthand of all digit, only matching integer, no decimal, no +- [\d\.]+
// [-\d\.]+ -333.40 is good
// \s - match the space
// ^beginning beginning of string >> only matches beginning but if it is the beginning, it wont match
// end$ match at the end, the end will be selected but not if there is something else in the end
// .+ is for everything , you need to escape the . if you want to match domain.com >> domain\.com


let text1 = '<!-- react-text: 8 -->SUN<!-- /react-text --><!-- react-text: 9 --> <!-- /react-text --><!-- react-text: 10 -->1<!-- /react-text -->'
let extractText = text1.match(/[0-9]/g);
console.log(extractText)