

import * as tf from '@tensorflow/tfjs-node';

import {WebsitePhishingService} from './WebsitePhishingService';
import * as argparse from 'argparse';
import * as utils from './utils';
import  createModel from './model';
import {falsePositiveRate} from './utils'



async function  run(epochs:number,batchSize:number,modelSavePath:string){

  const data = new WebsitePhishingService();
  await data.loadData();

  const testData = data.getTestData();
  
  
  const trainLogs:tf.Logs[] = [];
  const model = createModel(data.numFeatures);

  
  console.log('Running on test data...');
  const result =
      model.evaluate(testData.data, testData.target, {batchSize: batchSize});

  const lastTrainLog = trainLogs[trainLogs.length - 1];
  const testLoss = result[0].dataSync()[0];
  const testAcc = result[1].dataSync()[0];

  const probs = model.predict(testData.data) as tf.Tensor<tf.Rank> ;
  const predictions = utils.binarize(probs).as1D();

  const precision =
      tf.metrics.precision(testData.target, predictions).dataSync()[0];
  const recall =
      tf.metrics.recall(testData.target, predictions).dataSync()[0];
  const fpr = falsePositiveRate(testData.target, predictions).dataSync()[0];
  console.log(
      `Final train-set loss: ${lastTrainLog.loss.toFixed(4)} accuracy: ${
          lastTrainLog.acc.toFixed(4)}\n` +
      `Final validation-set loss: ${
          lastTrainLog.val_loss.toFixed(
              4)} accuracy: ${lastTrainLog.val_acc.toFixed(4)}\n` +
      `Test-set loss: ${testLoss.toFixed(4)} accuracy: ${
          testAcc.toFixed(4)}\n` +
      `Precision: ${precision.toFixed(4)}\n` +
      `Recall: ${recall.toFixed(4)}\n` +
      `False positive rate (FPR): ${fpr.toFixed(4)}\n`
  );
}




const parser = new argparse.ArgumentParser({
  description: 'TensorFlow.js-Node Website Phishing Example.',
  addHelp: true
});
parser.addArgument('--epochs', {
  type: 'int',
  defaultValue: 400,
  help: 'Number of epochs to train the model for.'
});
parser.addArgument('--batch_size', {
  type: 'int',
  defaultValue: 350,
  help: 'Batch size to be used during model training.'
})
parser.addArgument('--model_save_path', {
  type: 'string',
  defaultValue:"./model",
  help: 'Path to which the model will be saved after training.'
});
const args = parser.parseArgs();

run(args.epochs, args.batch_size, args.model_save_path);