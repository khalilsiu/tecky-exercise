/**
 * @license
 * Copyright 2018 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
import * as tf from '@tensorflow/tfjs-node';




/**
 * Shuffles data and label using Fisher-Yates algorithm.
 */
export async function shuffle(data:Int32Array[], label:Int32Array[]) {
  let counter = data.length;
  let temp :Int32Array;
  let index:number;
  while (counter > 0) {
    index = (Math.random() * counter) | 0;
    counter--;
    // data:
    temp = data[counter];
    data[counter] = data[index];
    data[index] = temp;
    // label:
    temp = label[counter];
    label[counter] = label[index];
    label[index] = temp;
  }
};

/**
 * Calculate the arithmetic mean of a vector.
 */
function mean(vector:number[]) {
  let sum = 0;
  for (const x of vector) {
    sum += x;
  }
  return sum / vector.length;
};

/**
 * Calculate the standard deviation of a vector.
 */
function stddev(vector:number[]) {
  let squareSum = 0;
  const vectorMean = mean(vector);
  for (const x of vector) {
    squareSum += (x - vectorMean) * (x - vectorMean);
  }
  return Math.sqrt(squareSum / (vector.length - 1));
};

/**
 * Normalize a vector by its mean and standard deviation.
 */
const normalizeVector = (vector:number[], vectorMean:number, vectorStddev:number) => {
  return vector.map(x => (x - vectorMean) / vectorStddev);
};

/**
 * Normalizes the dataset
 */
export function normalizeDataset(
              dataset:Int32Array[], 
              isTrainData = true, 
              vectorMeans:number[] = [], 
              vectorStddevs:number[] = []) {
      const numFeatures = dataset[0].length;
      let vectorMean: number;
      let  vectorStddev:number;

      for (let i = 0; i < numFeatures; i++) {
        const vector = dataset.map(row => row[i]);

        if (isTrainData) {
          vectorMean = mean(vector);
          vectorStddev = stddev(vector);

          vectorMeans.push(vectorMean);
          vectorStddevs.push(vectorStddev);
        } else {
          vectorMean = vectorMeans[i];
          vectorStddev = vectorStddevs[i];
        }

        const vectorNormalized =
            normalizeVector(vector, vectorMean, vectorStddev);

        vectorNormalized.forEach((value, index) => {
          dataset[index][i] = value;
        });
      }

      return {dataset, vectorMeans, vectorStddevs};
    };

/**
 * Binarizes a tensor based on threshold of 0.5.
 */
export function binarize(y:tf.Tensor<tf.Rank> , 
              threshold = 0.5) {
  tf.util.assert(
      threshold >= 0 && threshold <= 1,
      ()=>`Expected threshold to be >=0 and <=1, but got ${threshold}`);

  return tf.tidy(() => {
    const condition = y.greater(tf.scalar(threshold));
    return tf.where(condition, tf.onesLike(y), tf.zerosLike(y));
  });
}

/** 
 *  Calculate False Positives 
 */
export function falsePositives(yTrue: tf.Tensor<tf.Rank.R1>, yPred: tf.Tensor<tf.Rank.R1>) {
  return tf.tidy(() => {
    const one = tf.scalar(1);
    const zero = tf.scalar(0);
    return tf.logicalAnd(yTrue.equal(zero), yPred.equal(one))
        .sum()
        .cast('float32');
  });
}

/**
 * Calculate True Negatives
 */
export function trueNegatives(yTrue: tf.Tensor<tf.Rank.R1>, yPred: tf.Tensor<tf.Rank.R1>) {
  return tf.tidy(() => {
    const zero = tf.scalar(0);
    return tf.logicalAnd(yTrue.equal(zero), yPred.equal(zero))
        .sum()
        .cast('float32');
  });
}

/**
 * Calculate the rates of false positives.
 */
export function falsePositiveRate(yTrue: tf.Tensor<tf.Rank.R1>, yPred: tf.Tensor<tf.Rank.R1>) {
  return tf.tidy(() => {
    const fp = falsePositives(yTrue, yPred);
    const tn = trueNegatives(yTrue, yPred);
    return fp.div(fp.add(tn));
  });
}