import * as tf from '@tensorflow/tfjs-node';
import * as Papa from 'papaparse';
import fetch from 'node-fetch';
import * as utils from './utils';

const BASE_URL =
  'https://gist.githubusercontent.com/ManrajGrover/6589d3fd3eb9a0719d2a83128741dfc1/raw/d0a86602a87bfe147c240e87e6a9641786cafc19/';

  const TRAIN_DATA = 'train-data';
  const TRAIN_TARGET = 'train-target';
  const TEST_DATA = 'test-data';
  const TEST_TARGET = 'test-target';


export class WebsitePhishingService{ 
    readonly numFeatures = 30;
    readonly NUM_CLASSES = 30;
    private model: tf.LayersModel
    constructor(private dataset: Array<Int32Array[]> = [],
        private trainSize = 0,
        private testSize = 0,
        private trainBatchIndex = 0,
        private testBatchIndex = 0,){
        this.initialize();
        
        
    }

    async loadData() {
        this.dataset = await Promise.all([
          this.loadCsv(TRAIN_DATA), this.loadCsv(TRAIN_TARGET),
          this.loadCsv(TEST_DATA), this.loadCsv(TEST_TARGET)
        ]);
    
        let { dataset: trainDataset, vectorMeans, vectorStddevs } =
          utils.normalizeDataset(this.dataset[0]);
    
    
        this.dataset[0] = trainDataset;
    
        let { dataset: testDataset } = utils.normalizeDataset(
          this.dataset[2], false, vectorMeans, vectorStddevs);
    
        this.dataset[2] = testDataset;
        console.log("testDataset",testDataset)
    
        this.trainSize = this.dataset[0].length;
        this.testSize = this.dataset[2].length;
    
        utils.shuffle(this.dataset[0], this.dataset[1]);
        utils.shuffle(this.dataset[2], this.dataset[3]);
    
        console.log(`
          Train Size: ${this.trainSize}
          Test Size: ${this.testSize}
          Train Batch Index: ${this.trainBatchIndex}
          Test Batch Index: ${this.testBatchIndex}
          Number of classes: ${this.NUM_CLASSES}
        `)
      }

    getTestData() {

        const testData = Float32Array.from([].concat.apply([], this.dataset[2]));
        const testTarget = Float32Array.from([].concat.apply([], this.dataset[3]));
        console.log('testData',testData)
        return {
          data: tf.tensor2d(testData, [this.testSize, this.numFeatures]),
          target: tf.tensor1d(testTarget)
        };
      }

    async initialize(){
        console.log(this.model)
        this.model = await tf.loadLayersModel('file://./model/model.json');
    }

    predict(input:any){ // Use different data type according to your input.
        console.log("hello",this.model.predict(input))
        return this.model.predict(input);
    }

    async loadCsv(filename: string) {
        return new Promise<any[]>(async (resolve) => {
          const url = `${BASE_URL}${filename}.csv`;
          console.log(`  * Downloading data from: ${url}`);
          const res = await fetch(url);
        //   console.log("res",res)
          Papa.parse(res.body, {
            header: true,
            complete: async (results) => {
              const data = results['data'];
              const result = data.map(row => Object.keys(row).sort().map(key => parseFloat(row[key])))
              resolve(result);
            }
          })
        });
      };
}


let service = new WebsitePhishingService();
(async()=>{
    await service.loadData()
    let data = await service.getTestData().data
    console.log(await((service.predict(data)) as tf.Tensor<tf.Rank>).data())
})()