/**
 * @license
 * Copyright 2018 Google LLC. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */

import * as tf from '@tensorflow/tfjs-node';
import * as utils from './utils';
import * as Papa from 'papaparse';
import fetch from 'node-fetch';

// Website Phishing data constants:
const TRAIN_DATA = 'train-data';
const TRAIN_TARGET = 'train-target';
const TEST_DATA = 'test-data';
const TEST_TARGET = 'test-target';

const BASE_URL =
  'https://gist.githubusercontent.com/ManrajGrover/6589d3fd3eb9a0719d2a83128741dfc1/raw/d0a86602a87bfe147c240e87e6a9641786cafc19/';




/** Helper class to handle loading training and test data. */
export class WebsitePhishingDataset {

  readonly numFeatures = 30;
  readonly NUM_CLASSES = 30;
  constructor(
    private dataset: Array<Int32Array[]> = [],
    private trainSize = 0,
    private testSize = 0,
    private trainBatchIndex = 0,
    private testBatchIndex = 0,
  ) { }


  /** Loads training and test data. */
  async loadData() {
    this.dataset = await Promise.all([
      this.loadCsv(TRAIN_DATA), this.loadCsv(TRAIN_TARGET),
      this.loadCsv(TEST_DATA), this.loadCsv(TEST_TARGET)
    ]);

    let { dataset: trainDataset, vectorMeans, vectorStddevs } =
      utils.normalizeDataset(this.dataset[0]);

      console.log("hihi",this.dataset[0])

    this.dataset[0] = trainDataset;

    let { dataset: testDataset } = utils.normalizeDataset(
      this.dataset[2], false, vectorMeans, vectorStddevs);

    this.dataset[2] = testDataset;

    this.trainSize = this.dataset[0].length;
    this.testSize = this.dataset[2].length;

    utils.shuffle(this.dataset[0], this.dataset[1]);
    utils.shuffle(this.dataset[2], this.dataset[3]);

    console.log(`
      Train Size: ${this.trainSize}
      Test Size: ${this.testSize}
      Train Batch Index: ${this.trainBatchIndex}
      Test Batch Index: ${this.testBatchIndex}
      Number of classes: ${this.NUM_CLASSES}
    `)
  }

  getTrainData() {

    const trainData = Float32Array.from([].concat.apply([], this.dataset[0]));
    const trainTarget = Float32Array.from([].concat.apply([], this.dataset[1]));

    return {
      data: tf.tensor2d(trainData, [this.trainSize, this.numFeatures]),
      target: tf.tensor1d(trainTarget)
    };
  }

  getTestData() {

    const testData = Float32Array.from([].concat.apply([], this.dataset[2]));
    const testTarget = Float32Array.from([].concat.apply([], this.dataset[3]));

    return {
      data: tf.tensor2d(testData, [this.testSize, this.numFeatures]),
      target: tf.tensor1d(testTarget)
    };
  }

  async loadCsv(filename: string) {
    return new Promise<any[]>(async (resolve) => {
      const url = `${BASE_URL}${filename}.csv`;
      console.log(`  * Downloading data from: ${url}`);
      const res = await fetch(url);
      Papa.parse(res.body, {
        header: true,
        complete: async (results) => {
          const data = results['data'];
          const result = data.map(row => Object.keys(row).sort().map(key => parseFloat(row[key])))
          resolve(result);
        }
      })
    });
  };
}

