function getRandomInt(max)
{
    return Math.floor(max * Math.random());
}

let random = getRandomInt(100);

console.log(random);

let life = 3;



while (life > 0)
{
    let attempt = prompt("Enter your guess: ");
    if (life == 0)
    {
        console.log("You are dead.");
    }
    else if (attempt<random)
    {
        console.log(`Your guess is ${attempt}, larger please.`);
        life--;
        console.log(`You have ${life} lives left.`);
    }
    else if (attempt > random)
    {
        console.log(`Your guess is ${attempt}, smaller please.`);
        life--;
        console.log(`You have ${life} lives left.`);
    }
    else
    {
        console.log(`You are right, the answer is ${random}.`);
        break;
    }
}
if (life == 0)
{
    console.log("You are dead.");
}
