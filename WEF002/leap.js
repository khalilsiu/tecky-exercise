function isLeapYear(year)
{
    if (year%4==0)
    {
        if (year%100==0 && year%400==0)
        {
            return true;
        }
        else if (year%100==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

input = prompt("Enter the year that you want to check: ");
if (isLeapYear(input))
{
    console.log(input + " is a leap year.");
}
else
{
    console.log(input + " is not a leap year.");
}
