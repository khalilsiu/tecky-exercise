import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { listAllFileRecursive } from './listJsRevursive';
import * as expressSession from 'express-session';

// import * as path from 'path';


const app = express();

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}))

app.use(express.static('./'))
app.use(express.static('./mywebsite'))

//self created middleware
// app.use(req,res,next)=>{

// }

app.get('/', (req, res) => {
    res.send(`<link rel="stylesheet" href="index.css">this is the home page`)
    // console.log(req.session)
})

// every req should have a response, otherwise would be blocked
app.get('/login', (req, res) => {
    const { username, password } = req.query;
    if (req.session){
        if (!username || !password){
            res.end("please enter username and password");
        }
        else{
            req.session.username = username;
            req.session.password = password;
        }
    }
    // console.log(req.session);
    res.end('you are logged in');
})

// middleware
const checklogin = (req: Request, res: Response, next: NextFunction) => {
    // console.log("session", req.session)
    if (req.session && (!req.session.username || !req.session.password)) {
        res.end('no pw or username');
        return;
    }
    else {
        next();

    }

}

const logRequest = (req: Request, res: Response, next: NextFunction)=>{
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds() 
    if (req.session){
        console.log(`[${formatted_date}] Request ${req.path}`);
        next();
    }
    else{
        res.end('Not a valid request');
    }
}


app.get('/listFiles', checklogin, logRequest, async (req: Request, res: Response) => {
    const fileList = await listAllFileRecursive('./',"");
    // res.end only catches string
    res.json(fileList);


})

app.get('/listFiles/:ext', checklogin, logRequest, async (req, res) => {
    const { ext } = req.params;
    const extFileList = await listAllFileRecursive('./',"." + ext);
    res.json(extFileList);
})

app.get('/download', checklogin, logRequest, async (req, res) => {
    const { filePath } = req.query;
    // add the root in sendFile to set root
    res.sendFile(filePath, {root:"./"});
})



const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}`)
});
