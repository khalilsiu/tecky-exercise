import * as express from 'express';
import {Request,Response} from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as expressSession from 'express-session';

const app = express();

// Middleware: app.use (every request will run through them) 
// ensures that data in an url-encoded form can be parsed
app.use(bodyParser.urlencoded({extended:true}));
// ensures that JSON can be parsed
app.use(bodyParser.json());
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}))

// for css files, you put them into a file and send it to client
app.use(express.static('./mywebsite'))

// normally we use req to get data passed from client side: three properties
// query parameters are only used if we are using GET method
// more common to use query if parameters are optional in nature e.g. filter
app.get('/',function(req:Request, res:Response){
    const {name, location} = req.query;
    console.log(req.query);
    res.send(`<link rel="stylesheet" href="index.css"> Name is ${name}, location is ${location}`)

    // http://localhost:8080/?name=Tecky&location=hk.
})

// req.param is preferred way to get data from client if the parameters are mandatory
app.get('/name/:name/loc/:location',(req:Request, res:Response)=>{
    const {name, location} = req.params;
    if (req.session){
        req.session.name = name;
    }
    console.log(req.session);
    res.json({
        name,
        location,
        message: `Name is ${name}, Location is ${location}`
    })
})

app.post('/name', (req:Request, res:Response)=>{
    const {name, location} = req.body;
    res.end(`Name is ${name}, location is ${location}`);
})

app.get('/text',(req:Request, res:Response)=>{
    res.sendFile(path.join(__dirname,'index.js'));
    console.log('hahaha');
});

app.get('/text-redirect',(req:Request, res:Response)=>{
    res.redirect('/text');
});

// used to send static files e.g. images, videos and sound e.g. js and css, asset becomes the root path, how to access the route?


// catch-all to catch all routes and send the file of 404.html
app.use((req,res)=>{
    res.sendFile(path.join(__dirname,"404.html"));
});




const PORT = 8080;
app.listen(PORT,()=> {
    console.log(`Listening at http://localhost:${PORT}`);
})