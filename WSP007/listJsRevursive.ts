


import * as fs from 'fs';
import * as path from 'path';



function fsReadFileNamePromise(filePath: string) {
    // when you define a class, you put <T> behind the class name Node<T>
    // when you new a class, you specify the type of the T: new Node<number> 
    return new Promise<string[]>(function (resolve, reject) {
        fs.readdir(filePath, function (err: Error, files: string[]) {
            if (err) {
                reject(err);
            }
            else {
                resolve(files);

            }


        })
    })
}

function fsStatPromise(filePath: string) {
    return new Promise<fs.Stats>(function (resolve, reject) {
        fs.stat(filePath, function (err: Error, stat: fs.Stats) {
            if (err) {
                reject(err)
                return;

            }
            else {
                resolve(stat);

            }
        })
    })
}



export async function listAllFileRecursive(filePath: string,extension:string) {
    try {
        // recursive functions, creating an array here would renew the array again and again, making it unable to append
        // therefore defining the array as a parameter, would allow the recursive function to append to it every time it reaches the end
        let list:string[] = [];
        const fileNames: string[] = await fsReadFileNamePromise(filePath);
        for (let fileName of fileNames) {
            let fullPathName: string = path.join(filePath, fileName);
            let stat = await fsStatPromise(fullPathName);
            if (stat.isDirectory()) {
                // dont forget to await async functions
                // list.push would push empty array of directories, but use concat, would allow to concat arrays eliminating empty arrays 
                // concat will not create a new array
                list = list.concat(await listAllFileRecursive(fullPathName,extension));
            }
            else {
                if (fullPathName.endsWith(extension) && stat.isFile()) {
                    list.push(fullPathName);
                }
            }
            
        }
        // await fsWriteFilePromise("report.txt",fileNamesArray.join("\n"),{flag:'w'});
        return (list);

    }
    catch (err) {
        //console.log(filePath);
        return filePath;

    }
}




