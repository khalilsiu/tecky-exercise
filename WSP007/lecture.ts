import * as express from 'express';

const app = express();
//GET, POST

// if you hope there will be a request, use a callback
// req is the request info
// res is the response to client
app.get('/', function(req,res){
    res.send('Hi welcome')
})

app.get('/test', function(req,res){
    // the client's ip
    res.send('info')
    console.log(req.connection.remoteAddress)
})

app.get('/param/:asdf', function(req,res){
    // the client's ip
    res.send('info')
    console.log()
    console.log(req.connection.remoteAddress)
})

app.listen(8080, ()=>{
    console.log('attack me')
}); // 80 is for admin


