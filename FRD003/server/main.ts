import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as expressSession from 'express-session';
import * as Knex from 'knex';
import * as cors from 'cors';
import { BoardService } from './services/BoardService';
import { ScoreService } from './services/ScoreService';
import { ScoreRouter } from './routers/ScoreRouter';
import { BoardRouter } from './routers/BoardRouter';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false}
});

app.use(sessionMiddleware);

const boardService = new BoardService(knex);
const scoreService = new ScoreService(knex);

const boardRouter = new BoardRouter(boardService);
const scoreRouter = new ScoreRouter(scoreService);

app.use('/boards',boardRouter.router());
app.use('/scores',scoreRouter.router());


const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});

