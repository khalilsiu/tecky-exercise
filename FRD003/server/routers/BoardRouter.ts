import * as express from 'express';
import {Request,Response} from 'express';
import { BoardService } from '../services/BoardService';
import { Board } from '../services/models';

export class BoardRouter{

    constructor(private boardService:BoardService){}

    router(){
        const router = express.Router();
        router.get('/',this.list);
        router.get('/:id',this.get);
        router.post('/',this.post);
        router.put('/:id',this.put);
        router.delete('/:id',this.delete);
        return router;
    }

    private list = async (req:Request,res:Response)=>{
        try{
            const results:Board[] = await this.boardService.allBoards();
            res.json({isSuccess:true,data:results.map((row:Board)=>row.id)});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    private get = async (req:Request,res:Response)=>{
        try{
            const result:Board = await this.boardService.getBoard(req.params.id);
            if (!result){
                throw new Error('no id')
            }   
            res.json({isSuccess:true,data:result});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    private post = async (req:Request,res:Response)=>{
        try{
            const id = await this.boardService.createBoard(); 
            // console.log('id',id);   
            res.json({isSuccess:true,data:{id}});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    private put = async(req:Request,res:Response)=>{
        try{
            const {player,index} = req.body;
            await this.boardService
                            .nextStep(req.params.id,player,index);
            res.json({isSuccess:true});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
        
    }

 
    private delete = async (req:Request,res:Response)=>{
        try{
            await this.boardService.reset(req.params.id);
            res.json({isSuccess:true});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

}