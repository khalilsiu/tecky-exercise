import * as Knex from 'knex';

export class ScoreService{
    constructor(private knex:Knex){

    }

    allScores(){
        return this.knex.select('*').from('scores').orderBy('created_at');
    }

    addScore(winner:string,name:string,squares:string){
        return this.knex.insert({winner, name,squares}).into('scores');
    }

}

