import React from 'react';

export const NoMatch = () =>{
    return (
        <div>
            No page matches
        </div>
    )
}