import React from 'react';
import {Dispatch} from 'redux';
import { Button } from 'reactstrap';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';

interface IStopwatchProps{
    shoot: ()=> void
}

interface IStopwatchState{
    hasStarted: boolean
    hasPaused: boolean
    seconds: number
    laps: number [];
    
}

class Stopwatch extends React.Component<IStopwatchProps, IStopwatchState>{
    private intervalId? : NodeJS.Timeout
    constructor(props:IStopwatchProps){
        super(props);
        this.state = {
            hasStarted: false,
            hasPaused: false,
            seconds: 10,
            laps: []
        }
    }

    private handleStart = () =>{
        if (!this.state.hasStarted && !this.state.hasPaused){ // start
            this.intervalId = setInterval(()=>{
                this.setState({
                    hasStarted: true,
                    hasPaused: false,
                    seconds: this.state.seconds-0.1
                })
            },100)
        }else if (this.state.hasStarted && !this.state.hasPaused){ // pause
            this.setState({
                hasStarted: true,
                hasPaused: true,
                seconds: this.state.seconds
            })
            if (this.intervalId){
                clearInterval(this.intervalId)
            }
        }
        else{ // resume
            this.intervalId = setInterval(()=>{
                this.setState({
                    hasStarted: true,
                    hasPaused: false,
                    seconds: this.state.seconds-0.1
                })
            },100)
        }
    }

    private handleChange = (event: React.ChangeEvent <HTMLInputElement>)=>{
        this.setState({
            seconds: isNaN(parseInt(event.target.value))? 0 : parseInt(event.target.value)
        })
    }

    private handleLap = () => {
        if (this.state.hasStarted && !this.state.hasPaused){
            this.setState({
                laps : [...this.state.laps, this.state.seconds]
            })
        }
    }

    private handleReset = () =>{
        this.setState({
            hasStarted: false,
            hasPaused: false,
            seconds: 10,
            laps: []
        })
        if (this.intervalId){
            clearInterval(this.intervalId)
        }
    }

    componentWillUnmount(){
        if (this.intervalId){
            clearInterval(this.intervalId)
        }
    }

    public render () {
        return (
            <div>
                {
                    this.state.hasStarted? 
                    this.state.seconds.toFixed(1) : 
                    <input value={this.state.seconds} onChange={this.handleChange.bind(this)}/>
                    
                }
                {
                    this.state.seconds < 0.1 && this.state.hasStarted?
                    this.handleReset():
                    ''
                }
                <button onClick={this.handleStart.bind(this)}>{
                    !this.state.hasStarted && !this.state.hasPaused? 'Start' :
                    this.state.hasStarted && !this.state.hasPaused ? 'Pause': 'Resume'}</button>
                <button onClick={this.handleLap.bind(this)}>Lap</button>
                <button onClick={this.handleReset.bind(this)}>Reset</button>
                <ul>
                    {this.state.laps.map(lap=><li>{lap.toFixed(1)}</li>)}
                </ul>
                <Button color='primary' onClick={this.props.shoot}>Shoot</Button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch: Dispatch) =>{
    return {
        shoot: ()=> dispatch(push('/board'))
    }
}

export default connect(()=>{},mapDispatchToProps)(Stopwatch)