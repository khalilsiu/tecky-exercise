import React from 'react';
import Square from './Square'
import WinningModal from './WinningModal'
import './Board.css'
import {IRootState, ThunkDispatch} from './store'
import { connect } from 'react-redux';
import { nextStep, reset, getBoard } from './board/thunks';


interface IBoardProps {
    failedMsg?: string
    squares: Array<string | null>;
    oIsNext: boolean;
    getBoard: (id: number) => void;
    nextStep: (id: number, player: string, index: number) => void;
    reset: (id: number)=> void
    match:{
        params:{
            id:string
        }
    }
}


class Board extends React.Component<IBoardProps>{
    constructor(props: IBoardProps){
        super(props);
    }

    private renderSquares = (i: number) => {
        return <Square key={i} index={i} value={this.props.squares[i]} squareOnClick={this.handleClick.bind(this, i)} />
    }

    private handleClick = (i: number) => {
        const squares = this.props.squares.slice();
        if (!squares[i] && !this.calculateWinner()) {
            const player = this.props.oIsNext ? 'O' : 'X';
            this.props.nextStep(parseInt(this.props.match.params.id),player, i)
        }
    }

    private calculateWinner = () => {
        const squares = this.props.squares
        
        const winCases = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let winCase of winCases){
            let [a,b,c] = winCase;
            if (squares[a] && squares[a]===squares[b] && squares[a]===squares[c]){
                return squares[a]
            }
        }
        return null;
    }

    private replay = ()=>{
        this.props.reset(parseInt(this.props.match.params.id));
    }

    componentDidMount(){
        this.props.getBoard(parseInt(this.props.match.params.id))
    }

    public render() {
        const winner = this.calculateWinner();
        const status:string = winner? 
                `${winner} wins` : 
                `Next player is ${this.props.oIsNext? 'O': 'X'}` 
        let rows = [
            [0,1,2],
            [3,4,5],
            [6,7,8]
        ]
        return (
            <div>
                dius: {this.props.match.params.id }
                <div className='status'>{status}</div>
                {rows.map((row, i)=>{
                    return(
                        <div key={i} className='board-row'>
                            {row.map(col=>{
                                return (
                                    this.renderSquares(col)
                                )
                            })}
                        </div>
                    )
                })}
                {
                    winner && <WinningModal winner={winner} replay={this.replay}></WinningModal>
                }
                {
                    this.props.failedMsg && 'fuck!' + this.props.failedMsg
                }
            </div>
        )
    }

}

const mapStateToProps = (state: IRootState) =>{
    return {
        squares: state.board.squares,
        oIsNext: state.board.oIsNext,
        failedMsg: state.board.msg
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) =>{
    return {
        getBoard: (id: number) => dispatch(getBoard(id)),
        nextStep: (id: number, player: string, index: number) => dispatch(nextStep(id, player, index)),
        reset: (id: number)=> dispatch(reset(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board)