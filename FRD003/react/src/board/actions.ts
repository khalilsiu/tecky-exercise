

export function getBoardSuccess(squares:Array<string | null>){
    return {
        type: "GET_BOARD" as "GET_BOARD",
        squares: squares
    }
}

export function allBoardSuccess(previousBoardsIds: Array<number>){
    return {
        type: "ALL_BOARD" as "ALL_BOARD",
        previousBoardsIds
    }
}

export function nextStepSuccess (player: string, index: number){
    return {
        type: "NEXT_STEP" as "NEXT_STEP",
        player,
        index
    }
}

export function resetSuccess () {
    return {
        type: "RESET" as "RESET"
    }
}


export function failed(type: FAILED, msg: string){
    return {
        type,
        msg
    }
}


type BoardActionsCreator = typeof nextStepSuccess | 
                            typeof resetSuccess | 
                            typeof getBoardSuccess | 
                            typeof allBoardSuccess | 
                            typeof failed

type FAILED = "GET_BOARD_FAILED" | "NEXT_STEP_FAILED" | "RESET_FAILED" | "ALL_BOARD_FAILED" | "NEW_BOARD_FAILED" | "CREATE_BOARD_FAILED"

export type IBoardAction = ReturnType<BoardActionsCreator>