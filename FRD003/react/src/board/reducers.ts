import { IBoardState } from "./state";
import { IBoardAction } from "./actions";


const initialState = {
    squares: [],
    oIsNext: true,
    msg: ''
}

export const boardReducers = (state:IBoardState = initialState, action: IBoardAction)=>{

    switch(action.type){
        case "GET_BOARD":
            return {
                ...state,
                squares: action.squares,
                oIsNext: action.squares.filter((square)=>square).length % 2 == 0
            }
        case "NEXT_STEP":
            const {player, index} = action;
            const newSquares = state.squares.slice();
            newSquares[index] = player;
            return {
                ...state,
                squares: newSquares,
                oIsNext: !state.oIsNext
            }
        case "RESET":
            return {
                ...state,
                squares:[],
                oIsNext: true
            }
        case 'GET_BOARD_FAILED':
        case 'NEXT_STEP_FAILED':
        case 'RESET_FAILED':
        case 'CREATE_BOARD_FAILED':
            return {
                ...state,
                msg: action.msg
            }

        default:
            return state
    }
}

