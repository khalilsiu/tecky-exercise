
export interface IBoardState{
    squares: Array <string | null>
    oIsNext: boolean
    msg: string
}