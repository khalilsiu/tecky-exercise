import { Dispatch } from "redux";
import {IBoardAction, getBoardSuccess, failed, nextStepSuccess, resetSuccess} from './actions'
import { push, CallHistoryMethodAction } from "connected-react-router";

export function getBoard(boardId: number){
    return async (dispatch: Dispatch<IBoardAction>) =>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/boards/${boardId}`);
        const result = await res.json();

        if (result.isSuccess){
            dispatch(getBoardSuccess(result.data.squares as Array<string | null>))
        }else{
            dispatch(failed("GET_BOARD_FAILED",result.msg))
        }
    }
}

export function nextStep(boardId: number, player: string, index: number){
    return async (dispatch: Dispatch<IBoardAction>) =>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/boards/${boardId}`,{
            method: 'PUT',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                player: player,
                index: index
            })
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(nextStepSuccess(player, index))
        }else{
            dispatch(failed("NEXT_STEP_FAILED",result.msg))
        }
    }
}

export function reset(boardId: number){
    return async (dispatch: Dispatch<IBoardAction>) =>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/boards/${boardId}`,{
            method: 'DELETE'
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(resetSuccess());
        }else{
            dispatch(failed('RESET_FAILED',result.msg))
        }
    }
}

export function createBoard(){
    return async (dispatch: Dispatch<IBoardAction | CallHistoryMethodAction>)=>{
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/boards`,{
            method:'POST'
        });
        const result = await res.json();
        console.log(result)
        if (result.isSuccess){
            dispatch(push(`/board/${result.data.id}`))
        }else{
            dispatch(failed('CREATE_BOARD_FAILED',result.msg))
        }
    }
}

