import { IScoreState } from "./state";
import { IScoreAction } from "./actions";


const initialState = {
    scores: []
}

export const scoreReducers = (state:IScoreState = initialState, action: IScoreAction)=>{

    switch(action.type){
        case "ADD_WINNER":
            return {
                scores: [...state.scores,{
                    squares: action.squares,
                    name: action.name,
                    winner: action.winner
                }]
            }
        default:
            return state
    }
}

