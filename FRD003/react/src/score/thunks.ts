import { Dispatch } from "redux";
import {failed, addWinnerSuccess, IScoreAction} from './actions'


export function addWinner(name: string, squares: Array<string|null>, winner: string){
    return async (dispatch: Dispatch<IScoreAction>) =>{
        console.log("hi",name, squares, winner);
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/scores`,{
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                winner,
                name, 
                squares
            })
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(addWinnerSuccess(name, squares, winner))
        }else{
            dispatch(failed("ADD_WINNER_FAILED",result.msg))
        }
    }
}

export function getWinner(){
    return async (dispatch: Dispatch<IScoreAction>) =>{
        console.log('hiiiii')
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/scores`)
        const result = await res.json();
        if (result.isSuccess){
            console.log(result.data);
        }else{
            console.log('failed')
            dispatch(failed('GET_WINNER_FAILED', result.msg))
        }
    }
}