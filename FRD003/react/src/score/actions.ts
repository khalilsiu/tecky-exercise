

export function addWinnerSuccess (name: string, squares: Array<string|null>, winner: string){
    return {
        type: "ADD_WINNER" as "ADD_WINNER",
        name,
        squares,
        winner
    }
}


export function failed (type: FAILED, msg: string){
    return{
        type,
        msg
    }
}

type FAILED = "ADD_WINNER_FAILED" | "GET_WINNER_FAILED"

type ScoreActionsCreator = typeof addWinnerSuccess | typeof failed

export type IScoreAction = ReturnType<ScoreActionsCreator>