
export interface IScore{
    squares: Array <string | null>
    name: string
    winner: string
}

export interface IScoreState{
    scores: IScore[]
}