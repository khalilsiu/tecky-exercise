import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import {IBoardState} from './board/state';
import {IBoardAction} from './board/actions';
import {boardReducers} from './board/reducers';
import {IScoreState} from './score/state';
import {IScoreAction} from './score/actions';
import {scoreReducers} from './score/reducers';
import { logger } from 'redux-logger';
import { RouterState, connectRouter, routerMiddleware} from 'connected-react-router'
import { createBrowserHistory } from 'history';
import thunk, {ThunkDispatch} from 'redux-thunk'


export const history = createBrowserHistory()

declare global{
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}

export interface IRootState{
    board: IBoardState,
    score: IScoreState,
    router: RouterState
}

type IRootAction = IBoardAction | IScoreAction

const rootReducer = combineReducers({
    board: boardReducers,
    score: scoreReducers,
    router: connectRouter(history)
})

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState,IRootAction,{},{}>(rootReducer, composeEnhancers(applyMiddleware(logger), applyMiddleware(thunk), applyMiddleware(routerMiddleware(history))))