import React from 'react';
import './Square.css'
import { RouteComponentProps, withRouter } from 'react-router-dom'

interface ISquareProps {
    index: number
    value: string | null
    squareOnClick: () => void
}


class Square extends React.Component<ISquareProps & RouteComponentProps, {}>{

    constructor(props: ISquareProps & RouteComponentProps) {
        super(props);
    }

    public render() {
        return (
            <button className='square' onClick={this.props.squareOnClick}>{this.props.value}</button>
        )
    }
}

export default withRouter(Square)

