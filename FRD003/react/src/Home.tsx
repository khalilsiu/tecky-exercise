import React from 'react';
import { Button } from 'reactstrap';
import { ThunkDispatch } from './store';
import { createBoard } from './board/thunks';
import { connect } from 'react-redux';

interface IHomeProps{
    createBoard: () => void;
}

class Home extends React.Component<IHomeProps>{
    public render(){
        return (
            <div>
                This is the home page.
                <Button onClick={this.props.createBoard}> Create board </Button>
            </div>
        )

    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch)=>{
    return {
        createBoard: () => dispatch(createBoard())
    }
}

export default connect(()=>({}), mapDispatchToProps)(Home)