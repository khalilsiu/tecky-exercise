import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, InputGroup, Input } from 'reactstrap';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { addWinner } from './score/thunks';



interface IWinningModalProps{
    winner: string;
    replay: ()=>void;
    squares: Array<string | null>
    addWinner: (name: string, squares: Array<string | null>, winner: string) => void
}

interface IWinningModalState{
    name: string
}
class WinningModal extends React.Component<IWinningModalProps, IWinningModalState>{
    private intervalId?: NodeJS.Timeout
    // we do not need this to be stored onto the state, do not need to re-render every time
    constructor(props: IWinningModalProps){
        super(props);
        this.state = {
            name: ''
        }
    }

    public componentWillUnmount(){
        if(this.intervalId){
            clearInterval(this.intervalId)
        }
    }

    private handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>{
        this.setState({
            name: event.target.value
        })
    }

    private saveName = () =>{
        this.props.addWinner(this.state.name, this.props.squares, this.props.winner)
        this.props.replay();
    }

    public render(){
        return (
            <Modal isOpen={true}>
                <ModalHeader>Congratulations!</ModalHeader>
                    <ModalBody>
                        {this.props.winner} wins the game!
                    </ModalBody>
                            <Input type='text' value={this.state.name} onChange={this.handleChange} placeholder='Please enter your name:'/>
                    <ModalFooter>
                        <Button color='primary' onClick={this.saveName}>Save Name and Replay</Button>
                    </ModalFooter>
            </Modal>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        squares: state.board.squares
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) =>{
    return {
        addWinner: (name: string, squares: Array<string | null>, winner: string) => dispatch(addWinner(name, squares, winner))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(WinningModal)