import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Board from './Board';
import Stopwatch from './Stopwatch';
import { Provider } from 'react-redux';
import store from './store'
import { ConnectedRouter } from 'connected-react-router';
import {history} from './store'
import Scores from './Scores';
import Home from './Home'
import { About } from './About';
import { Link } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router';
import { NoMatch } from './NoMatch';

interface IGameState {
  page: string
}

export default class App extends React.Component<{}, IGameState> {

  constructor(props:{}){
    super(props);
    this.state = {
      page: '/board'
    }
  }

  private changePage = (page: string) =>{
    this.setState({
      page: page
    })
  }

  public render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div className="App">
            <nav className="nav-bar">
              <Link to='/' className='link'>Home</Link>
              <Link to='/scores' className='link'>Scores</Link>
              <Link to='/stopwatch' className='link'>Stop Watch</Link>
              <Link to='/about' className='link'>About</Link>
            </nav>
            <div>
              <Switch>
                <Route path="/" exact={true} component={Home} />
                <Route path="/board/:id" component={Board} />
                <Route path="/scores" component={Scores} />
                <Route path="/stopwatch" component={Stopwatch} />
                <Route path="/about" component={About} />
                <Route component={NoMatch}/>
              </Switch>
            </div>
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}
