import React from 'react';
import { ThunkDispatch } from './store';
import {getWinner} from './score/thunks'
import { connect } from 'react-redux';

interface IScoreProps{
    getWinner: ()=> void
}

class Scores extends React.Component<IScoreProps>{

    componentDidMount(){
        this.props.getWinner();
    }
    public render(){
        return (
            <div>
                Winners:
                <ul>
                    
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) =>{
    return {
        getWinner: ()=> dispatch(getWinner())
    }
}

export default connect(()=>({}), mapDispatchToProps)(Scores)