import * as fs from 'fs';

export class LoginService{
    private userRecords: { username: string, password: string }[];

    constructor(){
        if (!fs.readFileSync('report.json')){
            this.userRecords = []
        }
        else{
            this.userRecords = JSON.parse(fs.readFileSync('report.json','utf-8'));
        }
    }

    public async register(username:string, password: string){
        if (this.userRecords.find(x=>x.username===username)){
            return {"result":"username taken"}
        }
        else{
            this.userRecords.push({
                username: username,
                password: password})
            await fs.promises.writeFile('report.json',JSON.stringify(this.userRecords),{flag:'w'});
            return {"result":"you are registered"};
        }
    }

    public login(username:string, password: string){
        console.log("login is called");
        if(this.userRecords.find(x=>x.username === username) && this.userRecords.find(x=>x.password === password)){
            return true;
        }
        else{
            return false;
        }
    }


    
}   