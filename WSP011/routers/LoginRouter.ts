import * as express from 'express';
import {Request, Response, NextFunction} from 'express';
import { LoginService } from '../services/LoginService';

export class LoginRouter{
    private loginService: LoginService;

    constructor(){
        this.loginService = new LoginService();
    }
    public router(){
        const router = express.Router();
        router.post('/login', this.login);
        router.get('/login', this.checkLogin);
        return router;
    }

    public checkLogin = (req: Request, res:Response, next: NextFunction)=> {
        console.log("login is run")
        if (req.session && req.session.isLoggedIn){
            res.json({isLoggedIn: true});
        }
        else{
            res.json({isLoggedIn: false});
        }
    
    }

    private login = (req: Request, res:Response)=>{
        console.log("login router is run")
        let {username, password} = req.body
        if (req.session && this.loginService.login(username, password)){
            req.session.isLoggedIn = true;
            console.log(req.session.isLoggedIn)
            // res.json ({"result":"you are logged in"})
            res.redirect('/files.html');
        }
        else{
            res.json({"result": "wrong username or password"});
        }
    }
    
}