import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import {User, UserService} from './services/UserService';



const LocalStrategy = passportLocal.Strategy;

passport.use(new LocalStrategy(function(){
    async function(username,password,done){
        const user = await serService.getUsers();

        
    }
}))

passport.serializeUser(function(user:Users,done){ // to serialize the user to get the id, and deserialize for its associated info such that you do not need to store all of its data in an object 
    done(null, user.id) // is like a callback
})

// cookie is stored on the client side, server side according to the cookie stores the client's info: e.g. username, password, ....
// serialize and deserialize allows the server to only store the user id, to ensure privacy

passport.deserializeUser (async function(id, done) {
    const users = await userService.getUsers()
    const found = users.find((user)=> id == user.id);
    if(found){
        done(null,users);
    }else{
        done(new Error("User not Found"));
    }
});