import * as jsonfile from 'jsonfile';


interface User{
    id:number,
    username:string,
    password: string
}

export class UserService{
    
    async getUsers(){
       const users: User[] = await jsonfile.readFile('users')
       return users;
    }
}