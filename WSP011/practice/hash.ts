import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10; // salt is different every time, it generate salt 10 times

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};


export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    // since salt is different every time, you cannot compare the hashed plainpassword with the stored hashed password
    // you would need to use compare, it would be able to tell by the structure of the hashed password
    return match;
}