class EditEmail{
    constructor(){
        this.form = document.querySelector('#edit_email_form');
        this.email = document.querySelector('#edit_email');
        this.password = document.querySelector('#edit_password');
        this.form.addEventListener('submit', this.onSubmit);
        // to get data from method to method, we can use an attribute to store the return object!!!
        this.registrant = null;
        
    }

    populateEmail = async (registrant) => {
        this.registrant = registrant;
        this.email.value = registrant.email;
        this.password.value = registrant.password;

        

    }

    onSubmit = async(event)=>{
        event.preventDefault();
        // console.log(email.value);
        // console.log(password.value);
        
        console.log('this is run')
        if (this.registrant == null){
            return;
        }

        console.log(this.email.value);
        let res = await fetch (`/email/${this.registrant.id}`,{
            method :'PUT',
            headers : {
                'Content-Type': 'application/json'
              },
            body : JSON.stringify({
                email: this.email.value,
                password: this.password.value
            })
        
        });
        this.form.reset();
        res.json();
        window.listEmail.listEmail();
    }



    
}