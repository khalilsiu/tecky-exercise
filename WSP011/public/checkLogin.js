let checkLogin = async () =>{
    let res = await fetch ('/login');
    // is best to res.json from router
    let json = await res.json();
    console.log(json.isLoggedIn);
    if (json.isLoggedIn){
        location.href = 'viewer.html';
    }
    else{
        location.href = 'login.html';
    }
}

checkLogin();