class LoadDir {
    constructor(){
        this.currentPath = '/Users/anthonysiu/codes/tecky-exercises';
        this.loadDir();
        window.loadPath.loadPath(this.currentPath);
        this.fileContainer = document.querySelector('#file-container');
    }

    loadDir = async (filePath="")=>{
        
        let content = "";
        let res = await fetch('/directories',{
            method: 'POST',
            headers:{
                "Content-Type":"application/json; charset=utf-8"
            },
            body:JSON.stringify({
                path: filePath
            })
        });

        let directories = await res.json();
        for (let i = 0; i < directories.length; i++){
            content += 
            `<div class="directory" data-id="${i}">
            <a style="" href="page.html" class="inactiveLink" </a> <i class="fa fa-folder-open icons">${directories[i]}</i>
                
            </div>`
        }

        let fileDivs = await window.loadFile.loadFile(filePath);
        this.fileContainer.innerHTML = content + fileDivs;

        let dirLinks = document.querySelectorAll('.directory');
        Array.from(dirLinks).map((link)=>link.addEventListener('click', this.loadDirWithPath));
    }

    // this is how you can call the individual div by clicking
    loadDirWithPath = (event) =>{
        let dirLinks = document.querySelectorAll('.directory i');
        this.currentPath += "/"+dirLinks[event.currentTarget.dataset.id].innerHTML;
        this.loadDir(this.currentPath)
        window.loadPath.loadPath(this.currentPath);
        
    }
}


