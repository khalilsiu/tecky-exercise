class LoadFile {
    constructor(){
        this.loadFile();
        this.fileContainer = document.querySelector('#file-container');
    }

    loadFile = async (filePath="")=>{
        
        let content = "";
        let res = await fetch('/files',{
            method: 'POST',
            headers:{
                "Content-Type":"application/json; charset=utf-8"
            },
            body:JSON.stringify({
                path: filePath
            })
        });

        let files = await res.json();
        for (let i = 0; i < files.length; i++){
            content += 
            `<div class="files" data-id="${i}">
            <a style="" href= "" class="inactiveLink" </a>   <i class="fa fa-file icons">${files[i]}</i>
                
            </div>`
        }
        return content;
    }

    // this is how you can call the individual div by clicking
    loadDirWithPath = (event) =>{
        let dirLinks = document.querySelectorAll('.directory i');
        this.loadDir(dirLinks[event.currentTarget.dataset.id].innerHTML)
        
    }
}


