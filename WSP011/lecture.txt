async function loadPoints(){
    const res= await fetch('/acupoints');
    const points = await res.json();

    let html = '';
    for (let point of points){
        html += `<div class="point> ${point} </div>`;
    }

    document.querySelector('#root').innerHTML(html);
}

loadPoints();

// this function, the form will fetch data from the backend directly and display in webpage without any 閃
document querySelector('#addform').addEventLister async('submit', (event)=> {
    event.preventDefault(); <-- prevent the form from submitting
    const form = document.querySelector('#addform');
    await fetch('/acupoints',{
        method:'POST',
        headers:{
            "Content-Type":"application/json; charset=utf-8";
        },
        body: JSON.stringify({
            name: form.point_name.value
        })
    })
    loadPoints()
    // window.scrollTo(0,0) <-- the window will scroll to the top
})

// first launch the local host > html > css > script.js > /acupoints > fetch the json from the website
// fetch >> app >> router >> service >> router (res.json) >> app (res.json) >> fetch(res.json) to display "web surfing to server at the background"
// the fetched data is being looped over and put inside the id "root"

<form id="addform"> // from the above JS
<form action = "/acupoints" method = "POST"> (GET will transform into a query in the address) / in the path means getting the file from the highest level (the root folder will be the file that you sent using app.use(express.static()))
    name: <input type="text" name ="username" required> (the name will be assigned to the key of the query or body), required will not make sure input is present
    <input type = "submit">
</form>


<form action\ = "/tongue" method="POST" enctype="multipart/form-data"> <-- 閃
    pic: <input type="file" name="pic_name" required>
    <input type='submit'>
</form>

// submit POST >> app.ts app.use >> router req.body.name >> service.push >> router redirect >> reloads
reloads >> fetch >> >> app >> router >> service >> router (res.json) >> app (res.json) >> fetch(res.json)

if (req.body.name ==  null || req.body.name.trim() == "") <-- since having space is not null, trimming spaces if ""

in POST >> by default is xxurlencoded, does not support image, you need to add <form 
bodyParser does not recognize photos, therefore needs another middleware: multer

Backend TS
export class TongueRouter{ 
    public router(){
        const upload = multer({dest:'uploads/'}); <-- will upload the file to this path
        const router = express.Router();

        router.post('/', upload.single('pic_name'), this.tongue) <-- is relative to the name at the input above, can upload multiple
        return router;
    }
    private tongue(){

    }
}

Frontend JS
document querySelector('#uploadform').addEventLister async('submit', (event)=> {  <- 不閃
    event.preventDefault(); <-- prevent the form from submitting
    const form = document.querySelector('#uploadForm');
    const formData = new FormData(form);
        await fetch('/tongue',{
        method:'POST',
        body: formData
        })
    })
    
})

Checking login: since we cannot ask all the paths to

in html, there are only post and get, for put and delete, must need 不閃
delete, there should be no body, you do not need content header in the JS file