import * as express from 'express';
import { LoginRouter } from './routers/LoginRouter';
import * as expressSession from 'express-session';
import * as bodyParser from 'body-parser';
import { DirectoryRouter } from './routers/DirectoryRouter';
import { FileRouter } from './routers/FilesRouter';



const app = express();

const loginRouter = new LoginRouter();
const dirRouter = new DirectoryRouter();
const fileRouter = new FileRouter();


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static(__dirname+'/public'))
app.use(expressSession({
    secret: 'File listing app',
    resave: true,
    saveUninitialized: true
}))

app.use('/', loginRouter.router());
app.use('/',dirRouter.router());
app.use('/',fileRouter.router())


const PORT = 8080;
app.listen(PORT, ()=>console.log(`Listening at http://localhost:${PORT}`));