import * as express from 'express'
import * as http from 'http'
import * as socketIO from 'socket.io'

const app = express()
app.use(express.static(__dirname + '/public'))
//traditional server would use express
const server = new http.Server(app)
//io would upgrade it
const io = socketIO(server)

io.on('connection',function(socket){
    socket.on('msg',(msg)=>{
        socket.emit('got-msg',msg)
    })
})

const PORT = 8080
server.listen(PORT,()=>{
    console.log(`listening port ${PORT}`)
})