
import * as readline from 'readline';
import {listAllJsRecursive, fsWriteFilePromise} from './ex1'

// import * as util from 'util';
// import * as fs from 'fs';
import * as os from 'os';



const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const readCommand = () =>{
    readLineInterface.question("Please choose read the report(1) or run the benchmark(2): ", async(answer: string)=>{
        const option = parseInt(answer,10);
        console.log(`Option ${answer} chosen`);
        if(option == 1){
            await readTheReport();
        }else if(option == 2){
            await runTheBenchmark();
        }else{
            console.log("Please input 1 or 2 only");
        }
        readCommand();
    })
}

readCommand();



// 1. Do it yourself
// only async function needs to be await-ed, to get in order into a promise chain, other normal sync function dont need await to get in order.
async function runTheBenchmark(){
    try{
        let start = new Date().getTime();
        await listAllJsRecursive("./",[]);
        let endOne = new Date().getTime();
        console.log(`${Math.abs(endOne-start)/1000}s`);
        for (let i = 0; i < 100; i ++){
            await listAllJsRecursive("./",[]);
        }
        let endHundred = new Date().getTime();
        console.log(`${Math.abs(endHundred-start)/1000}s`);
        for (let i = 0; i < 10000; i ++){
            await listAllJsRecursive("./",[]);
        }
        let endTenThousand = new Date().getTime();
        console.log(`${Math.abs(endTenThousand-start)/1000}s`);
    }
    catch(e){
        console.error(e);
    }
    
}

// 2. Use util.promisify - original, the function accepts a callback with has NodeJS style (err, callback)
// *** however this cannot promisify function with other structures, another one is event emitter
    // e.g. readLineInterface
// function promisifiedReadLine(question: string){
//     return new Promise((resolve)=>{
//         readLineInterface.question(question, async(answer:string)=>{
//             resolve(answer);
//         })
//     })
// }
// let readdirPromUtil = util.promisify(fs.readFile);
// 3. use fs.promise module
// fs.promises.readdir()

// therefore sync will return 0.
// let start = new Date().getTime();
// listAllJsRecursive("./");
// let end = new Date().getTime();
// console.log(Math.abs(end-start));

async function readTheReport(){
    try{
        let startMemory:number = os.freemem();
        await listAllJsRecursive("./",[]);
        let endMemory: number = os.freemem();
        await fsWriteFilePromise("report.txt","start: "+ startMemory +"\n",{flag:'a'});
        await fsWriteFilePromise("report.txt","end: "+ endMemory+"\n",{flag:'a'})
        await fsWriteFilePromise("report.txt","difference: "+ (endMemory-startMemory)+"\n",{flag:'a'})
        
        console.log("start: ", startMemory, "end: ", endMemory, "difference: ",endMemory-startMemory);
    }
    catch(e){
        console.error(e);
    }
}

