


import * as fs from 'fs';

// const dijkstraQuote1 = "Computer science is no more about computers than astronomy is about telescopes.\n";
// const dijkstraQuote2 = "Simplicity is prerequisite for reliability.\n";


// This creates a readfile function that creates a Promise object that can use then and catch.
// instead of a callback function in any asynchronous functions
// function fsWriteFilePromise(file:string, content: string,options:{flag:string}):Promise<{}>{
//     return new Promise(function(resolve, reject){
//         fs.writeFile(file, content,options, function(err:Error){
//             if(err){
//                 reject(err);
//             }
//             resolve();
//         })
//     })
// }



// console.log("Step 1");
// fsWriteFilePromise('quotes-dijkstra.txt',
//                     "Simplicity is prerequisite for reliability.\n",
//                     {flag:'w'})
                // the returnValue here is the data being return e.g. buffer from fs.readfile, but there is no return value for writefile
//             .then(function(returnValue){
//                    console.log("Step 3");;
//                   console.log("Return the return value");
//                   return returnValue;
//             })
//             // the parameter will be the return value of the previous handler
//             .then(function(returnValue){
//                 console.log("Step 4");
//                 console.log("returnValue here same as return value of step 3 promise")
//                 return returnValue;
//                 // the error will be the error in any of the previous handlers
//             }).catch(function(error:string){
//                 console.log("Step 5");
//                 console.log(error);
//             });
// console.log("Step 2");


// const promise1 = Promise.resolve(3);
// const promise2 = Promise.resolve(5);

// const promiseAll = Promise.all([promise1, promise2])

// promiseAll.then(function(values){
//     console.log(values[0])
//     console.log(values[1])
// })

function readFile(file:string, options?:{flag:string}){
    return new Promise <Buffer> (function(resolve, reject){
        fs.readFile(file, options, function(err:Error, data:Buffer){
            if(err){
                reject(err);
            }
            resolve(data);
        })
    })
}

// async and await greatly enhance the readability of the async code compared to the above then, catch
// this is the preferred way of writing async codes, under the hood, readFile is still producing Promise 
// will it run if 
// async function asyncReadFile(){
//     try{
//         const data = await readFile('haha.txt');
//         console.log(data.toString('utf-8'));
//         const data2 = await readFile('quotes-dijkstra.txt');
//         console.log(data2.toString('utf-8'));
//     }catch(err){
//         console.log(err);
//     }
// }

// asyncReadFile();


// this, you are executing both promises at the same time and wait until both of them are being responded first before the next task
async function asyncReadParallel(){
    try{
        // this code is run before the console.log hahaha, since there is no await, is being treated as normal function
        // await only has effect on Promise object but not others like console.log
        console.log("uuuuu")
        // this is being run last
        const [data,data2] = await Promise.all([readFile('quotes-dijkstra.txt'), readFile('quotes.txt')]);
        console.log(data.toString('utf-8'))
        console.log(data2.toString('utf-8'));
    }
    catch(err){
        console.log(err);
    }
}

asyncReadParallel();
// this is being run next
console.log("hahaha");