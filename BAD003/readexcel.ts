import * as XLSX from 'xlsx';
import {Client} from 'pg';

interface USER{
    username:string;
    password: string;
    level: string;
}

interface FILE {
    name: string;
    content: string;
    isFile: number;
}

let workbook = XLSX.readFile('BAD003-exercise.xlsx')
let client  = new Client ({
    user: 'anthonysiu',
    host: 'localhost',
    database: 'anthonysiu',
    password: 'anthonysiu'
});

let user_sheet = workbook.Sheets[workbook.SheetNames[0]]
let file_sheet = workbook.Sheets[workbook.SheetNames[1]]
let category_sheet = workbook.Sheets[workbook.SheetNames[2]];
let users:USER[] = XLSX.utils.sheet_to_json(user_sheet);
let files: FILE[] = XLSX.utils.sheet_to_json(file_sheet);
let categories: {name:string}[] = XLSX.utils.sheet_to_json(category_sheet);



(async()=>{
    await client.connect()

    for (let user of users){
        const text = 'INSERT INTO "user" (username, password, level) VALUES ($1, $2, $3) RETURNING *';
        const values = [user.username, user.password, user.level]
        const res = await client.query(text, values)
        console.log(res.rows)
    }

    for (let file of files){
        const text = 'INSERT INTO "file" (name, isfile, content) VALUES ($1, $2, $3) RETURNING *';
        const values = [file.name, file.isFile, file.content];
        const res = await client.query(text, values);
        console.log(res.rows)
    }
    for (let category of categories){
        const res = await client.query(`INSERT INTO "file" (name, isfile, content) VALUES ('${category.name}')`);
        console.log(res.rows)
    }
    await client.end();
})();