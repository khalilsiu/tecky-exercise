


function square(x){
    return x**2;
}


// function invocation !

let result = square(5);

console.log(result);

let result2 = square(3);

console.log(square(result2));


// y = f(x) = x**2 + 1

function squareAndPlusOne(x){
    let squaredX = square(x);
    return squaredX+1;
}

let result0 = squareAndPlusOne(3);
console.log(square(result0));


// Another way of declaring function
// anonymous function same as the function square(x) declaration
// difference is, for anonymous functions is assigned to a variable directly, non-anon functions can be defined everywhere 
//let square = function(x){
//    return x**2
//}


//Arrow function
// function(x) = >>>> (x)=>
// usually same
/*let cube = (x)=>{
    return x**3;
}*/

// can be reduced to this, only if there is one line only
// only have 1 param, can remove parenthesis; if have 0 or more than 1, need parenthesis
// curly braces, can only be removed if there is only 1 return line

let cube = x => x**3;

let result3 = cube(3);
console.log(result3);


let cubeAndPlus1 = (x)=>{
    let cubedX = cube(x);
    return cubedX+1;
}


function sumOfArray(array){
    let sum = 0;
    for(let num of array){
        sum += num;
    }
    return sum;
}

console.log(sumOfArray([4,6,7,3,8,9,2,1]));

// you can add more parameters by , 
let productOfTwoNumbers = (first, second)=>{
    return first * second;
}

console.log(productOfTwoNumbers(10,20));

// if you do not give parameter, no number is given, will give NaN
// can set default variables

/* function square(x = 5){
    return x**2;
}*/
console.log(square());


console.log(Math.random());

console.log(Math.cos(Math.PI / 2));

console.log(Math.ceil(Math.E));

console.log(parseInt("1234"));
console.log(parseFloat("1234.1234"));

// setTimeout: functions after x seconds

// setTimeout(function(){
//     console.log("This sentence prints only after 2 seconds");
// }, 2000);

// // setInterval: run functions at an interval
// // e.g. set yahoo finance to update every 2s

// setInterval(console.log("this sentence prints every 2s"), 2000);

// const intervalId = setInterval(function(){
//     console.log("This sentence prints every 2s");
// }, 2000);

// // you can stop setInterval at the console: clearInterval(intervalId)
// console.log(intervalId);


// alert("you are wrong!");

// const name = prompt("May i know your name?");

// const loveToCode = confirm("do you love to code?");

if (true){
    // x is in the scope of the if statement
    // x is a local variable
    // if you remove let, it becomes a global variable even inside a function, clause
    let x = 1234;

}

// reason why we use let instead of var, is because var treats variables in if and for clauses global
for (var i = 0; i< 3; i++){
    console.log(i);
}

