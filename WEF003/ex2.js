


function rnaTransciption(dnaStrand){
    rnaStrand = [];
    for (let dna of dnaStrand){
        switch (dna){
            case "G":
                rnaStrand.push("C");
                break;
            case "C":
                rnaStrand.push("G");
                break;
            case "T":
                rnaStrand.push("A");  
                break; 
            case "A":
                rnaStrand.push("T");  
                break;

        }

    }
    return rnaStrand;
}

console.log(rnaTransciption(["C","G","A","T","Y"]));