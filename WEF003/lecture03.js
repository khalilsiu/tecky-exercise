
// Bind
// without binding: this here points to the window of the browser, "this" value changes in different places in the code
// 2 functionalities:
// 1. Attach the first argument to "this" variable: if it is an object, this.value > will give the value; you can bind 123 > this = 123
// 2. Assigns arguments afterwards to the parameters
function sum(first,second, third, fourth){
    console.log(third, fourth);
    return `Name is ${this.name}: Number is ${first + second}`;
}

let addTo5 = sum.bind({name: "Gordon"}, 5);
console.log(addTo5(16));

let always20 = sum.bind({name: "Gordon"}, 5, 15);
console.log(always20(12,23));