

// you have to set a variable to equal to the return value of the outer function (which is the inner function), such that when you call it, the variable can be defined once, and changed.
function accumulator(){
    let sum = 0;
    return function(num){
        sum += num;
        return sum;
    };
}

let acc = accumulator();

console.log(acc(10));
console.log(acc(10));
console.log(acc(10));

// you can return an object, and object can be assigned functions as well!!!
// this is a higher order function: a function that can create different objects(functions), to carry out actions
// very much similar to classes
function calculator(brand){
    let sum = 0;
    if (brand === "casio"){
        return {
            add: function(num){
                sum = sum + num;
            },
            minus: function(num){
                sum = sum - num;
            },
            value: function(){
                return sum;
            }
        }
    }
    else {
        return {
            add: function(num){
                sum = sum + num;
            },
            minus: function(num){
                sum = sum - num;
            },
            multiply: function(num){
                sum = sum * num;
            },
            value: function(){
                return sum;
            }
        }
    }
}

const calc = calculator("sharp");
calc.add(10);
calc.multiply(2);
calc.minus(15);
console.log(calc.value());