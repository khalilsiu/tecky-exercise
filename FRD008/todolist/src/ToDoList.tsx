import React from 'react';
import { Button, ListGroup, Input, Row, Col, FormGroup, Form } from 'reactstrap';
import { ThunkDispatch, IRootState } from './store'
import { submitItem, getList, deleteItem, editItem, editCompleteStatus } from './list/thunks';
import { connect } from 'react-redux';
import { IList } from './list/state';
import { ToDoItem } from './ToDoItem';



export interface IListProps {
    submitItem: (item: string) => void;
    list: IList[]
    getList: (status?: string) => void
    deleteItem: (i: number | null) => void
    editItem: (id: number, item: string) => void
    editCompleteStatus: (id: number, completed: boolean) => void
    match: {
        params: {
            status: string
        }
    }
    location: string
    
}
interface IListState {
    value: string
}

export class ToDoList extends React.Component<IListProps, IListState> {

    constructor(props: IListProps) {
        super(props);
        this.state = {
            value: ''
        }
    }

    private handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            value: event.target.value
        })
    }

    private resetInput = () => {
        this.setState({
            value: ''
        })
    }

    private handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        this.resetInput()
        this.props.submitItem(this.state.value)
    }

    componentDidMount(){
        this.props.getList(this.props.match.params.status)
    }


    componentDidUpdate(prevProps: IListProps) {
        if (this.props.location !== prevProps.location) {
        this.props.getList(this.props.match.params.status)
        }
      }

    public render() {
        return (
            <div>
                    <FormGroup>
                        <Form onSubmit={this.handleSubmit}>
                            <Input value={this.state.value} onChange={this.handleChange}/>
                        </Form>
                    </FormGroup>
                    <ListGroup>
                        {this.props.list.map((item, i) => {
                            return (
                                <ToDoItem key={i} getList={this.props.getList} status={this.props.match.params.status} order={i} item={item} deleteItem={this.props.deleteItem} editItem={this.props.editItem} editCompleteStatus={this.props.editCompleteStatus} />
                            )
                        })}
                    </ListGroup>
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        list: state.list.list
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        submitItem: (item: string) => dispatch(submitItem(item)),
        getList: (status?: string) => dispatch(getList(status)),
        deleteItem: (i: number | null) => dispatch(deleteItem(i)),
        editItem: (id: number, item: string) => dispatch(editItem(id, item)),
        editCompleteStatus: (id: number, completed: boolean) => dispatch(editCompleteStatus(id, completed))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList)