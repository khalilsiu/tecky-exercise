import { MockStoreEnhanced } from "redux-mock-store";
import configureMockStore from 'redux-mock-store'
import { IRootState } from "../store";
import { ThunkDispatch } from "../store";
import thunk from "redux-thunk";
import fetchMock from 'fetch-mock'
import { loginSuccess, loginFailed } from "./actions";
import { push } from "connected-react-router";
import { login, loginFacebook } from "./thunks";


describe('Auth thunks',()=>{
    let store: MockStoreEnhanced<IRootState, ThunkDispatch>;
    beforeEach(()=>{
        const mockStore = configureMockStore<IRootState, ThunkDispatch>([thunk])
        store = mockStore();
    })

    it('should login successfully',async ()=>{
        const result = {
            token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJwZXBlIn0.klIQbP3ySfBvMgxO8vP3YHTNen-YDiOUK1HFfBHBvCs'
        }

        fetchMock.post(`${process.env.REACT_APP_API_SERVER}/login`,{
            body: result,
            status: 200
        })

        const expectedActions = [
            loginSuccess(),
            push('/')
        ]

        await store.dispatch(login('username','password'))
        expect(store.getActions()).toEqual(expectedActions);
        expect((global as any).localStorage.getItem('token')).toEqual(result.token)
    })

    it('should login with facebook successfully',async ()=>{
        const result = {
            token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJwZXBlIn0.klIQbP3ySfBvMgxO8vP3YHTNen-YDiOUK1HFfBHBvCs',
            accessToken: 'EAAP6uKMlTtIBAF8RUYA3ME4RCP1iZCb1kG17qZAZAxyZCqbmb8VWG7N1sVHxhbNIOB03GYk9iUvZApyPgJjxybd8tvqZC0ZB9sQIu7XOf5vB75l2TFF5i45UZBodlWQiIIKfZA51fAMCwLesYoEDjZB9UZCbDYD9iO6y0ccIOAYvuC24QLOWOGzw5wAGdEN1r86qbOLMoVeVTFNdwZDZD'
        }

        fetchMock.post(`${process.env.REACT_APP_API_SERVER}/login/facebook`,{
            body: result
        })

        const expectedActions = [
            loginSuccess(),
            push('/')
        ]

        await store.dispatch(loginFacebook(result.accessToken))
        expect(store.getActions()).toEqual(expectedActions)
        expect((global as any).localStorage.getItem('token')).toEqual(result.token)
    })

    it('should fail to login with facebook',async ()=>{
        const result = {
            accessToken: 'abcde',
            msg: 'wrong token'
        }
        fetchMock.post(`${process.env.REACT_APP_API_SERVER}/login/facebook`,{
            body: result,
            status: 401
        })

        const expectedActions=[
            loginFailed(result.msg)
        ]
        await store.dispatch(loginFacebook(result.accessToken))
        expect(store.getActions()).toEqual(expectedActions);
        expect(store.getActions()[0].msg).toEqual(expectedActions[0].msg)
    })

    afterEach(()=>{
        fetchMock.restore()
    })
})