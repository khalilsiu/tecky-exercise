

export function loginSuccess() {
    return {
        type: "LOGIN" as "LOGIN"
    }
}

export function loginFailed(msg: string) {
    return {
        type: "LOGIN_FAILED" as "LOGIN_FAILED",
        msg: msg
    }
}

export function logoutSuccess() {
    return {
        type: "LOGOUT" as "LOGOUT"
    }
}

type AuthActionCreators = typeof loginSuccess | typeof loginFailed | typeof logoutSuccess 

export type IAuthActions = ReturnType<AuthActionCreators>;