import {IAuthState} from './state';
import { IAuthActions } from './actions';


const initialState = {
    isAuthenticated: (localStorage.getItem('token')!=null),
    msg: ''
}

export function authReducer(state: IAuthState = initialState, action: IAuthActions){
    switch(action.type){
        case "LOGIN":{
            return {
                ...state,
                isAuthenticated: true,
                msg: ''
            }
        }
        case "LOGOUT":{
            return {
                ...state,
                isAuthenticated: false,
                msg: ''
            }
        }
        case "LOGIN_FAILED":{
            return {
                ...state,
                isAuthenticated: false,
                msg: action.msg
            }
        }
        default:{
            return state
        }
    }
}