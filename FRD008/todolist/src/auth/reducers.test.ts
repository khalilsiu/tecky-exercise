import { IAuthState } from "./state";
import { authReducer } from "./reducers";
import { loginSuccess } from "./actions";


describe('Auth Reducers',()=>{
    let initialState: IAuthState;

    beforeEach(()=>{
        initialState = {
            isAuthenticated: false,
            msg: ''
        }
    })

    it('should login successfully',()=>{
        let finalState = authReducer(initialState,loginSuccess())
        expect(finalState).toEqual({
            ...initialState,
            isAuthenticated: true,
            msg: ''
        })
    })
})