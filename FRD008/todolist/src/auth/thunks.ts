import { Dispatch } from "redux";
import { loginFailed, loginSuccess, IAuthActions, logoutSuccess } from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";


export function login(username: string, password: string){
    return async (dispatch: Dispatch<IAuthActions|CallHistoryMethodAction>)=>{
        const res =  await fetch (`${process.env.REACT_APP_API_SERVER}/login`,{
            method: 'POST',
            headers:{
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                username, password
            })
        });
        const result = await res.json();
        if (res.status != 200){
            dispatch(loginFailed(result.msg))
        }else{
            localStorage.setItem('token',result.token);
            dispatch(loginSuccess())
            dispatch(push('/'))
        }
    }
}

export function loginFacebook(accessToken: string){
    console.log(accessToken)
    return async(dispatch: Dispatch<IAuthActions|CallHistoryMethodAction>)=>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/login/facebook`,{
            method: 'POST',
            headers:{
                "Content-Type":"application/json; charset=utf-8"
            },
            body: JSON.stringify({accessToken})
        })
        const result = await res.json();
        if(res.status!==200){
            dispatch(loginFailed(result.msg))
        }else{
            localStorage.setItem('token',result.token);
            dispatch(loginSuccess())
            dispatch(push('/'))
        }
    }
}

export function logout(){
    return async (dispatch: Dispatch<IAuthActions|CallHistoryMethodAction>)=>{
        dispatch(logoutSuccess())
        localStorage.removeItem('token')
        dispatch(push('/'))
    }
}