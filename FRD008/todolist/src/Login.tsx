import React from 'react';
import {Form, FormGroup, Label, Input, Alert, Button} from 'reactstrap'
import {IRootState, ThunkDispatch} from './store'
import { connect } from 'react-redux';
import {login, loginFacebook} from './auth/thunks'
import ReactFacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login';


interface ILoginState{
    username:string,
    password:string
}

interface ILoginProps{
    login: (username:string,password:string)=> void
    msg:string
    loginFacebook: (accessToken: string)=> void
}


class Login extends React.Component<ILoginProps,ILoginState> {
    constructor(props:ILoginProps){
        super(props);
        this.state = {
            username:"",
            password:""
        }
    }
    private onChange = (field: 'username' | 'password', e: React.FormEvent<HTMLInputElement>) => {
        const state:any = {};
        state[field] = e.currentTarget.value;

        this.setState(state);
    }

    private login = ()=>{
        const {username,password} = this.state;
        if(username && password){
            this.props.login(username,password);
        }
    }
    private fBOnClick(){
        return null;
    }

    private fBCallback =  (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
        if (userInfo.accessToken) {
          this.props.loginFacebook(userInfo.accessToken);
        }
        return null;
      }

    public render() {
        return (
        <Form>
            <FormGroup>
            <Label for="username">Username</Label>
            <Input type="email"  
                value={this.state.username}
                onChange= {this.onChange.bind(this,'username')} 
                placeholder="Type in Username" />
            </FormGroup>
            <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" 
                value={this.state.password} 
                onChange= {this.onChange.bind(this,'password')}
                placeholder="Type in Password" />
            </FormGroup>
            <FormGroup>
                <div className="fb-button">
                    <ReactFacebookLogin
                        appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
                        fields="name,email,picture"
                        onClick={this.fBOnClick}
                        callback={this.fBCallback}
                    />  
                </div>
            </FormGroup>
            { this.props.msg?
                <Alert color="danger">
                    {this.props.msg}
                </Alert>:""
             }
            <Button onClick={this.login}>Submit</Button>
        </Form>
        );
    }
}

const mapStateToProps = (state:IRootState)=>({
    msg: state.auth.msg
});

const mapDispatchToProps = (dispatch:ThunkDispatch)=>({
    login: (username:string,password:string)=>dispatch(login(username,password)),
    loginFacebook: (accessToken: string)=> dispatch(loginFacebook(accessToken))
})

export default connect(mapStateToProps,mapDispatchToProps)(Login)