declare const global:any;

global.localStorage = {
    getItem(key:string){
        return this[key];
    },
    setItem(key:string,value:string){
        this[key] = value;
    },
    removeItem(key:string){
        delete this[key];
    }
}