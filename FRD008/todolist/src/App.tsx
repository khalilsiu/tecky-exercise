import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { history } from './store'
import './App.css';
import ToDoList from './ToDoList';
import { Provider } from 'react-redux';
import store from './store';
import { ConnectedRouter } from 'connected-react-router';
import { Route, Link, Switch } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute'
import Login from './Login'
import Logout from './Logout'
import { Container } from 'reactstrap';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Container>
          <Logout />
          <div className="todocontainer">
            <div className="todobox">
              <Switch>
                <Route path='/login' component={Login}></Route>
                <PrivateRoute path="/" exact={true} component={ToDoList}></PrivateRoute>
                <PrivateRoute path="/:status" component={ToDoList}></PrivateRoute>
              </Switch>
              <Link to="/">All</Link>
              <Link to="/active">Active</Link>
              <Link to="/completed">Completed</Link>
            </div>
          </div>
        </Container>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
