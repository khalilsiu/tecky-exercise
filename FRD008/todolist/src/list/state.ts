
export interface IList {
    id: number | null,
    item: string | null,
    completed: boolean | null
}

export interface IListState{
   list: IList[]
}