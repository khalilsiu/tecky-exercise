import { IListState, IList } from "./state";


export function submitItemSuccess(id: number, item: string){
    return {
        type: "ADD_ITEM" as "ADD_ITEM",
        id,
        item
    }
}

export function getListSuccess(list:IList[]){
    return {
        type: "GET_LIST" as "GET_LIST",
        list: list
    }
}

export function deleteItemSuccess(id: number){
    return {
        type: "DELETE_ITEM" as "DELETE_ITEM",
        id
    }
}

export function editItemSuccess(id: number, item: string){
    return {
        type: "EDIT_ITEM" as "EDIT_ITEM",
        id,
        item
    }
}

export function editStatusSuccess(id: number, completed: boolean){
    console.log(id)
    return {
        type: "EDIT_STATUS" as "EDIT_STATUS",
        id,
        completed
    }
}

export function failed(type: FAILED, msg: string){
    return {
        type,
        msg
    }
}

type IListActionCreator = typeof submitItemSuccess | typeof getListSuccess | typeof deleteItemSuccess | typeof editItemSuccess |typeof editStatusSuccess | typeof failed
type FAILED = "ADD_ITEM_FAILED" | "GET_LIST_FAILED" | "DELETE_ITEM_FAILED" | "EDIT_ITEM_FAILED" | "EDIT_STATUS_FAILED"

export type IListAction = ReturnType<IListActionCreator>