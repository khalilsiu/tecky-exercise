import {IListState, IList} from './state';
import {IListAction} from './action'

const initialState = {
    list: [] as IList[]
}

export const listReducers = (state: IListState = initialState, action: IListAction) =>{
    switch(action.type){
        case "ADD_ITEM":{
            return {
                list : [...state.list, {
                    id: action.id,
                    item: action.item,
                    completed: false
                }]
            }
        }
        case "GET_LIST":{
            return {
                list: action.list
            }
        }
        case "DELETE_ITEM":{
            const list = state.list.filter(item => item.id !== action.id)
            return {
                list : list
            }
        }
        case "EDIT_ITEM":
            const newList = state.list.map(item => item.id === action.id? item={id: action.id, item: action.item, completed: item.completed}: item)
            return {
                list: newList
            }
        case "EDIT_STATUS":
            console.log(action.completed)
            const list = state.list.map(item => item.id === action.id? item={id: action.id, item: item.item, completed: action.completed}: item)
            return{
                list: list
            }
        default:
            return state;
    }
}