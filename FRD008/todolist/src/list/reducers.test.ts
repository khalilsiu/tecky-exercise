import { IListState, IList } from "./state";
import { listReducers } from "./reducers";
import { submitItemSuccess, getListSuccess, editStatusSuccess } from "./action";


describe('List Reducers',()=>{
    const list: IList = {
        id: 1,
        item: "take medicine",
        completed: true
    }
    let initialState: IListState;

    beforeEach(()=>{
        initialState = {
            list: [list]
        }
    })

    it('should add item',()=>{
        let finalState = listReducers(initialState, submitItemSuccess(2, 'buy milk'))
        expect(finalState).toEqual({
            list: [...initialState.list,{
                id: 2,
                item: 'buy milk',
                completed: false
            }]
        })
    })

    it('should get list',()=>{
        let finalState = listReducers(initialState,getListSuccess([list]))
        expect(finalState).toEqual({
            list: [list]
        })
    })

    it('should edit status',()=>{
        let id=3;
        let completed=true
        let finalState = listReducers(initialState, editStatusSuccess(id, completed))
        const list = initialState.list.map(item => item.id === id? item={id: id, item: item.item, completed: completed}: item)
        expect(finalState).toEqual({
            list: list
        })
    })

})