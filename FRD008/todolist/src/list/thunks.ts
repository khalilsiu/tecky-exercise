import {Dispatch} from 'redux';
import { IListAction, submitItemSuccess, failed, getListSuccess, deleteItemSuccess, editItemSuccess, editStatusSuccess } from './action';

export function submitItem(item: string){
    return async (dispatch:Dispatch<IListAction>) =>{
        console.log(item)
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/todolist`,{
            method: 'POST',
            headers:{
                'Content-Type':'application/json',
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({item})
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(submitItemSuccess(result.id, item));
        }else{
            dispatch(failed("ADD_ITEM_FAILED", result.msg))
        }
    }
}

export function getList(status?: string){
    console.log(status)
    return async (dispatch: Dispatch<IListAction>) =>{
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/todolist/${status}`,{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        });
        const result = await res.json();
        console.log(result);
        if (result.isSuccess){
            dispatch(getListSuccess(result.list))
        }else{
            dispatch(failed("GET_LIST_FAILED", result.msg))
        }
    }
}

export function deleteItem(id: number | null){
    console.log(id)
    return async (dispatch: Dispatch<IListAction>)=>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/todolist/${id}`, {
            method: 'DELETE',
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json();
        console.log(result)
        if(result.isSuccess){
            dispatch(deleteItemSuccess(result.id))
        }else{
            dispatch(failed("DELETE_ITEM_FAILED",result.msg))
        }
    }
}

export function editItem(id: number, item:string){
    console.log(id, item)
    return async (dispatch: Dispatch)=>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/todolist/${id}`,{
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                item
            })
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(editItemSuccess(id, item))
        }else{
            dispatch(failed("EDIT_ITEM_FAILED", result.msg))
        }
    }
}

export function editCompleteStatus(id: number, completed: boolean){
    console.log(id, completed)
    return async (dispatch: Dispatch)=>{
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/todolist/status/${id}`,{
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                completed
            })
        })
        const result = await res.json();
        if (result.isSuccess){
            dispatch(editStatusSuccess(result.id, completed))
        }else{
            dispatch(failed("EDIT_STATUS_FAILED",result.msg))
        }
    }
}