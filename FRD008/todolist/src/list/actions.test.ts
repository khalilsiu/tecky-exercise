import { submitItemSuccess, getListSuccess, deleteItemSuccess } from "./action";


describe('List Actions',()=>{
    it('should submit item successfully',()=>{
        expect(submitItemSuccess(1,'take medicine')).toEqual({
            type:"ADD_ITEM",
            id: 1,
            item: 'take medicine'
        })
    })

    it('should get list successfully',()=>{
        expect(getListSuccess([{
            id: 1,
            item: 'take medicine',
            completed: true
        }])).toEqual({
            type: "GET_LIST",
            list: [{
                id: 1,
                item: 'take medicine',
                completed: true
            }]
        })
    })

    it('should delete item successfully',()=>{
        expect(deleteItemSuccess(1)).toEqual({
            type: "DELETE_ITEM",
            id: 1
        })
    })

})
