import {IRootState} from './store';
import {RouteProps, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import React from 'react';

interface IPrivateRouteProps extends RouteProps {
    isAuthenticated: boolean
}

// component is passed from RouteProps
// high order component: renders a component based on some pre-conditions
const PurePrivateRoute = ({ component, isAuthenticated,...rest}: IPrivateRouteProps) =>{
    const Component = component;
    if(Component == null) {
        return null;
    }
    let render:(props: any) => JSX.Element
    // if isAuthenticated, render component
    if(isAuthenticated){
        render = (props: any) =>{
            return <Component {...props} />
        }
    }else{
        // else redirect to login
        render = (props: any) =>{
            return <Redirect to={{
                pathname:'/login',
                state: { from: props.location}
            }}/>
        }
    }
    return <Route {...rest} render={render}/>
}

export const PrivateRoute = connect((state: IRootState)=>({
    isAuthenticated: state.auth.isAuthenticated
}))(PurePrivateRoute)