import React from 'react';
import Button from 'reactstrap/lib/Button';
import { IList } from './list/state';
import { Input, ListGroupItem, Row, Col } from 'reactstrap';
import { ThunkDispatch } from './store';
import { getList } from './list/thunks';
import { connect } from 'react-redux';



interface IListItemProps {
    item: IList
    order: number
    deleteItem: (id: number) => void
    editItem: (id: number, item: string) => void
    editCompleteStatus: (id: number, completed: boolean) => void
    getList:(status: string) => void
    status: string
}
interface IListItemState {
    value: string
    editMode: boolean
}

export class ToDoItem extends React.Component<IListItemProps, IListItemState>{
    constructor(props: IListItemProps) {
        super(props);
        console.log(this.props.item)
        this.state = {
            value: this.props.item.item as string,
            editMode: false,
        }
    }

    private changeEditMode = () => {
        this.setState({
            editMode: !this.state.editMode
        })
    }

    private handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            value: event.target.value
        })
    }

    private handleCheckChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        await this.props.editCompleteStatus(this.props.item.id as number, !this.props.item.completed)
        await this.props.getList(this.props.status)
    }

    private renderEditView = () => {
        return (
            <div>
                <input type='text' defaultValue={this.state.value} onChange={this.handleInputChange} />
                <Button onClick={() => {
                    this.changeEditMode()
                    this.props.editItem(this.props.item.id as number, this.state.value)
                }}>Submit</Button>
            </div>
        )
    }

    private renderDefaultView = () => {
        return (
            <div>
                <div onDoubleClick={this.changeEditMode}>{this.props.item.item}</div>
            </div>

        )
    }

    public render() {
        return (
            <div>
                <ListGroupItem>
                    <Row>
                        {console.log(this.props.item)}
                        <Col sm="2"><Button onClick={this.props.deleteItem.bind(this, this.props.item.id as number)}>X</Button></Col>
                        <Col sm="1"><Input checked={this.props.item.completed as boolean} onChange={this.handleCheckChange} type='checkbox'/></Col>
                        <Col sm="3">{this.props.order+1}</Col>
                        <Col sm="3">
                            {this.state.editMode && this.props.item.item ?
                                this.renderEditView()
                                :
                                this.renderDefaultView()
                            }
                        </Col>
                        <Col sm="3">{this.props.item.completed ? "Completed" : "Active"}</Col>
                    </Row>




                </ListGroupItem>
            </div>
        )
    }


}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        getList: (status: string) => dispatch(getList(status))
    }
}
export default connect(()=>({}), mapDispatchToProps)(ToDoItem)
