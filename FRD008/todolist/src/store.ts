import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import { IListState } from "./list/state";
import { IListAction } from "./list/action";
import { listReducers } from "./list/reducers";
import { authReducer} from './auth/reducers'
import { logger } from "redux-logger";
import { createBrowserHistory } from "history";
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction
} from "connected-react-router";
import thunk, { ThunkDispatch } from "redux-thunk";
import { IAuthState } from "./auth/state";
import { IAuthActions } from "./auth/actions";

export const history = createBrowserHistory();

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  list: IListState;
  auth: IAuthState;
  router: RouterState;
}

type IRootAction = IListAction | CallHistoryMethodAction | IAuthActions

const rootReducer = combineReducers<IRootState>({
  list: listReducers,
  auth: authReducer,
  router: connectRouter(history)
});

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  composeEnhancers(
    applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  )
);
