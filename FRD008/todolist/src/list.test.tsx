import * as TestRenderer from 'react-test-renderer'
import React from 'react'
import {ToDoList, IListProps} from './ToDoList'
import { ToDoItem } from './ToDoItem';

jest.mock('./ToDoItem')

describe('List Element',()=>{
    let props: IListProps
    beforeEach(()=>{
        (ToDoItem as any as jest.Mock).mockReturnValue(<div>I am a List</div>)
        props = {
            submitItem: jest.fn((item: string) => null),
            list: [{
                id: 1,
                item: 'fuck',
                completed: false
            }],
            getList: jest.fn((status?: string) => null),
            deleteItem: jest.fn((i: number | null) => null),
            editItem: jest.fn((id: number, item: string) => null),
            editCompleteStatus: jest.fn((id: number, completed: boolean) => null),
            match: {
                params: {
                    status: 'active'
                }
            },
            location: 'active'
            }
    })

    it('should contain a list',()=>{
        const testRenderer = TestRenderer.create(<ToDoList {...props}/>)
        const testInstance = testRenderer.root
        expect(testInstance.findAllByType(ToDoItem)).toBeDefined()
    })
})