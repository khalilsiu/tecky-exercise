import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    await knex("users").del()
    return await knex.insert({
        username: 'pepe',
        password: '$2b$10$vsJtO.gyl59Vwz78kqvrXOuHpWO07zZekeJmn37oYs0k2ehKf0vAS',
        email: 'pepe@gmail.com'
    }).into('users')
};
