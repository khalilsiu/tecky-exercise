import * as Knex from 'knex';
import { hashPassword } from '../hash';

export class UserService{
    constructor(private knex: Knex){

    }
    getUser(username: string){
        return this.knex.select('*').from('users').where('username',username)
    }
    getUserByEmail(email:string){
        return this.knex.select('*').from('users').where('email',email);
    }

    async createUser(email:string){
        const randomString = Math.random().toString(36)
        return this.knex.insert({
            email,
            username: email.split("@")[0],
            password: await hashPassword(randomString)
        }).into('users').returning("*");
    }

}