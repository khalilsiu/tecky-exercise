import * as knex from 'knex'

export default class ToDoListService{
    constructor(private knex: knex){

    }

    public createItem = async(item: string) =>{
        return (await this.knex
                    .insert({
                        'item-name': item,
                        'completed': false
                    })
                    .into('to-do-list')
                    .returning('id'))[0]

    }

    public getList = async(status?: string)=>{
        console.log(status)
        if (status==='active'){
            return (await this.knex
                        .select('*')
                        .from('to-do-list')
                        .where('completed',false)
                        .orderBy('id'))
        }else if (status==='completed'){
            return (await this.knex
                .select('*')
                .from('to-do-list')
                .where('completed',true)
                .orderBy('id'))
        }else{
            return (await this.knex
                .select('*')
                .from('to-do-list')
                .orderBy('id'))
        }
                
    }

    public deleteItem = async (id: number)=>{
        return (await this.knex
                        .delete('*')
                        .from('to-do-list')
                        .where('id', id)
                        .returning('id'))[0]
    }

    public editItem = async (id: number, item: string)=>{
        return (await this.knex('to-do-list')
                        .update({
                        'item-name': item
                        })
                        .where('id', id)
                        .returning('id'))[0]
    }

    public editStatus = async (id: number, completed: boolean)=>{
        return (await this.knex('to-do-list')
                        .update({
                            'completed': completed
                        })
                        .where('id', id)
                        .returning('id'))[0]
    }

}