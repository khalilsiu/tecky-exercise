import * as express from 'express'
import ToDoListService from '../services/ToDoListService';


export default class ToDoListRouter{
    constructor(private toDoListService: ToDoListService){
        
    }

    public router (){
        const router = express.Router();
        router.get('/:status', this.getList)
        router.post('/', this.createItem);
        router.delete('/:id', this.deleteItem)
        router.put('/:id', this.editItem)
        router.put('/status/:id', this.editStatus)
        return router
    }

    private createItem = async (req: express.Request, res: express.Response) =>{
        try{
            let id = await this.toDoListService.createItem(req.body.item);
            res.json({isSuccess:true, id: id});
        }catch(e){
            res.json({isSuccess: false, msg: e.toString()})
        }
    }

    private getList = async (req: express.Request, res: express.Response) =>{
        try{
            console.log("hello",req.params.status)
            let list = await this.toDoListService.getList(req.params.status);
            res.json({isSuccess:true, list: list.map(item=>({
                id: item.id,
                item: item["item-name"],
                completed: item.completed
            }))});
        }catch(e){
            res.json({isSuccess: false, msg: e.toString()})
        }
    }

    private deleteItem = async (req: express.Request, res: express.Response) =>{
        try{
            let id = await this.toDoListService.deleteItem(req.params.id);
            console.log(id)
            res.json({isSuccess:true,id: id});
        }catch(e){
            res.json({isSuccess: false, msg: e.toString()})
        }
    }

    private editItem = async (req: express.Request, res: express.Response) =>{
        try{
            let id = await this.toDoListService.editItem(req.params.id, req.body.item);

            res.json({isSuccess:true,id: id});
        }catch(e){
            res.json({isSuccess: false, msg: e.toString()})
        }
    }

    private editStatus = async (req: express.Request, res: express.Response) =>{
        try{
            let id = await this.toDoListService.editStatus(req.params.id, req.body.completed);
            res.json({isSuccess:true,id: id});
        }catch(e){
            res.json({isSuccess: false, msg: e.toString()})
        }
    }
}