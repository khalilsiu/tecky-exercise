import * as express from 'express';
import {Request,Response} from 'express';
import { UserService } from '../services/UserService';
import * as jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import {checkPassword} from '../hash';
import fetch from 'cross-fetch'

export default class UserRouter{
    constructor(private userService: UserService){

    }
    public router(){
        const router = express.Router();
        router.post('/login', this.post);
        router.post('/login/facebook', this.loginFacebook)
        return router;
    }

    private post = async (req: Request, res: Response)=>{
        try{
            console.log(req.body.username)
            if (!req.body.username || !req.body.password) {
                res.status(401).json({msg: "Wrong username/password"})
                return;
            }
            const {username, password} = req.body;
            const user = (await this.userService.getUser(username))[0];
            if (!user || !(await checkPassword(password, user.password))){
                res.status(401).json({msg: "Wrong username/password"})
                return;
            }
            const payload = {
                id: user.id,
                username: user.username
            };
            // encode the payload using jwtSimple with jwtSecret
            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            res.json({
                token: token
            })
        }catch(e){
            console.log(e);
            res.status(500).json({msg: e.toString()})

        }
    }

    private loginFacebook = async (req: Request, res: Response)=>{
        try{
            if (!req.body.accessToken){
                res.status(401).json({msg: "Wrong access token"})
                return
            }
            const {accessToken} = req.body
            const fetchResponse = await fetch (`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`)
            const result = await fetchResponse.json();
            if(result.error){
                res.status(401).json({msg: 'Wrong access token'})
                return;
            }
            let user = (await this.userService.getUser(result.email))[0]

            if (!user){
                user = (await this.userService.createUser(result.email))[0]
            }

            const payload = {
                id: user.id,
                username: user.username
            }

            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            res.json({token: token})
        }catch(e){
            console.log(e);
            res.status(500).json({msg: e.toString()})

        }
    }
}