import * as express from 'express';
import * as Knex from 'knex';
import * as bodyParser from 'body-parser';
import ToDoListRouter from './router/ToDoListRouter';
import ToDoListService from './services/ToDoListService';
import cors = require('cors');
import * as passport from 'passport'
import {isLoggedIn} from './guard'
import {UserService} from './services/UserService'
import UserRouter from './router/UserRouter'
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(passport.initialize());
import './passport'

const toDoListService = new ToDoListService(knex)
const toDoListRouter = new ToDoListRouter(toDoListService)
export const userService = new UserService(knex);
const userRouter = new UserRouter(userService)
app.use('/todolist',isLoggedIn,toDoListRouter.router());
app.use('/', userRouter.router());

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
