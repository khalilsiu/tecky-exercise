import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTableIfNotExists('to-do-list',(table)=>{
        table.increments();
        table.string('item-name');
        table.boolean('completed');
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists('to-do-list');
}

