import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTableIfNotExists('users', table=>{
        table.increments();
        table.string('username');
        table.string('password');
        table.string('email')
        table.timestamps(false,true);
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists('users')
}

