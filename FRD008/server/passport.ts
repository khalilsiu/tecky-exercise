import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';
import {userService} from './main'
import jwt from './jwt'

const JWTStrategy = passportJWT.Strategy
const {ExtractJwt} = passportJWT;

// runs every time guard is triggered that it has to call api, verifies the user using the token
passport.use(new JWTStrategy({
    secretOrKey: jwt.jwtSecret,
    // this specifies where the server is getting the token from
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, async(payload, done)=>{
    const user = await userService.getUser(payload.username);
    if (user){
        // after this, req.user will contain the information of the user
        return done(null, user);
    } else{
        return done(new Error("User not Found"), null)
    }
}))