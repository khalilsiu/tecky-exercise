// running ts-node/register, we do not need to install ts-node and typescript as global package > just run npm install
require('ts-node/register');
require('./app'); 

// we need to run index.js as a proxy, we can also run directly by typing ts-node app.ts
// need to make sure that ts-node and typescript are installed
