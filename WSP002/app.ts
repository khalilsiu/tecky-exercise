
// import axios from 'axios'

// axios.get();

// import * as Express from 'express';

// Express()

// const module = require('some-packages'). default


// let color: string = "red";
let color = "red";
// it knows as well: Type inference
console.log(typeof(color));

let numArray = [1,2,3,4];
console.log(typeof(numArray));


function some_function_call_returning_null(){
    return null;
}
let val = some_function_call_returning_null();
// error below
// val.abc();

// need to check for error
// if (val){
//     val.abc();
// }

// private and protected, both cant be obtained outside of the class, even console.logging
// protected can be used for subclass, private cannot be used anywhere outside of class
class Person{

    public name: string;
    constructor(name:string){
        this.name = name;
    }
}

class Student extends Person{
    private currentClass: string;
    constructor(name: string, currentClass: string){
        super(name);
        this.name = `Student ${this.name}`;
        this.currentClass = currentClass;
        
    }
    study(){
        console.log(this.currentClass);
    }

}

const p = new Student("John","F.1");
p.study();

// error
console.log(p.name);

function add(a: number, b: number): number{
    return a+b;
}

console.log(add(1,2));


// enum is for limited data types e.g. directions, cards
// = 1, = 2 are for the order of them
enum Direction{
    East = 1,
    South,
    West,
    North
}

function turnTo(direction:Direction){
    if (direction == Direction.North){
        console.log("This is east");
    }
    return direction != Direction.East;
}

turnTo(4);

// T would parse the input and interpret its type
// any wont bother checking its type
function identity<T>(arg: T): T{
    return arg;
}

console.log(typeof(identity(23423)));