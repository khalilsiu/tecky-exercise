
import printNumbers from './printNumbers'
import filter from './filter'
import {Person, goToBar} from './Person'

// still necessary for manual mock
// just that the filter mock function is being imported from the __mock__ folder
jest.mock('./filter');
jest.useFakeTimers()

it('Testing printNumbers',()=>{
    // this can be removed
    // if the returning value of the mocking function is used again and again, it is repetitive to write the logic of "mockReturnValue" again and again. 
    // (filter as jest.Mock).mockReturnValue([1,3,5]);

    console.log = jest.fn()

    printNumbers();
    expect(filter).toBeCalledTimes(1);
    jest.advanceTimersByTime(5000)
    // since timeout functions are not run immediately, this will fail
    expect(console.log).toBeCalledWith([4,5,6])
})

// it("Testing goToBar",()=>{
//     const john = new Person(15);
//     const peter = new Person(20);
//     const johnSpy = jest.spyOn(john, 'drink')
//     const peterSpy = jest.spyOn(peter,'drink');

//     goToBar([john, peter])
//     expect(johnSpy).not.toBeCalled()
//     expect(peterSpy).toBeCalled();
// })

it("Testing goToBar with a teenager",async ()=>{
    const john = new Person(15);
    const peter = new Person(20);
    const johnSpy = jest.spyOn(john, 'drink')
    const peterSpy = jest.spyOn(peter,'drink');

    try{
        await goToBar([john,peter])
    }
    catch(err){
        expect(err).toEqual([peter])
    }

    // const adults = goToBar([john,peter]);
    expect(johnSpy).not.toBeCalled();
    expect(peterSpy).not.toBeCalled();
    // expect(adults).rejects.toEqual([peter]);
})

it("Testing goToBar with all adult",()=>{
    const sam = new Person(25);
    const peter = new Person(20);
    const samSpy = jest.spyOn(sam, 'drink')
    const peterSpy = jest.spyOn(peter,'drink');

    const adults = goToBar([sam,peter]);
    expect(samSpy).toBeCalled();
    expect(peterSpy).toBeCalled();
    expect(adults).resolves.toEqual([sam, peter]);
})