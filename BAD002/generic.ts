
function identity(arg:any){
    return arg;
}

let number = identity(1234)
// number is of type any, you do not know if it is a number

function identityGeneric<T>(arg:T):T{
    return arg
}
// you hope the input is of ANY type AND the SAME as the output type 

let numberGeneric = identityGeneric(1234)
// numberGeneric is a number

class IdentityClass<S>{
    constructor(public value:S){

    }
}

let identityObject = new IdentityClass("123")
identityObject.value

class Converter<A,B>{
    constructor(public from:A, public to:B){

    }
}

let converter = new Converter("12",12)
// there are 2 types for this class