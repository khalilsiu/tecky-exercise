import printNumbers from './printNumbers'
import filter from './filter'

jest.mock('./filter')

it('Testing printNumbers',()=>{
    (filter as jest.Mock).mockReturnValue([1,3,5]);

    console.log = jest.fn()

    printNumbers();
    expect(filter).toBeCalledTimes(1);
    expect(console.log).toBeCalledWith([1,3,5])
})