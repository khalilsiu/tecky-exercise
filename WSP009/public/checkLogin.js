// let checkLogin = async () =>{
//     let res = await fetch ('/login');
//     // is best to res.json from router
//     let json = await res.json();
//     console.log(json.isLoggedIn);
//     if (json.isLoggedIn){
//         location.href = 'viewer.html';
//     }
//     else{
//         location.href = 'login.html';
//     }
// }

// checkLogin();

window.onload = function(){
    const searchParams = new URLSearchParams(window.location.search);
    // built in method to get the URLSearchParams
    const errMessage = searchParams.get('error');
    // get it and assign to a variable

    if(errMessage){
        const alertBox = document.createElement('div');
        alertBox.classList.add('alert','alert-danger');
        alertBox.textContent = errMessage;
        document.querySelector('#error-message').appendChild(alertBox);
    }else{
        document.querySelector('#error-message').innerHTML = "";
    }
}