class LoadPath{
    constructor(){
        this.path = document.querySelector('#file-path');
        this.loadPath();
        console.log(this.currentPath)

    }

    loadPath = async (filePath)=>{
        // console.log(await res.json())
        this.path.innerHTML = `Index of ${filePath}`;

    }
    
}
// async function loadPath(filePath = ""){
//     const res = await fetch('/current-path', {
//         method: 'POST',
//         headers:{
//             "Content-Type":"application/json; charset=utf-8"
//         },
//         body: JSON.stringify({
//             path: filePath
//         })
//     });
//     const path = await res.json();

//     console.log(document.querySelector('#file-path').innerHTML);
//     document.querySelector('#file-path').innerHTML = `Index of ${path}`;
// }

// loadPath();

// async function loadDir(filePath = ""){
//     const res = await fetch('/directories', {
//         method: 'POST',
//         headers:{
//             "Content-Type":"application/json; charset=utf-8"
//         },
//         body: JSON.stringify({
//             path: filePath
//         })
//     });
//     const dirs = await res.json()

//     let html = '';
//     for (let dir of dirs){
//         html += `
//         <div class="directory"> 
//                 <i class="fa fa-folder-open"></i>
//                 ${dir} 
//         </div>`
//     }
//     document.querySelector("#file-container").innerHTML = html;
//     let directories = document.querySelectorAll('.directory');
//     for (let directory of directories){
//         directory.addEventListener('click', (event)=>{
//             let index = event.target.innerHTML.indexOf("i>")
//             let filePath = event.target.innerHTML.slice(index+2).trim();
//             loadPath(filePath);
//             loadDir(filePath);
//         })
// }
// }
// loadDir();


// async function loadFile(filePath = ""){
//     const res = await fetch('/files', {
//         method: 'POST',
//         headers:{
//             "Content-Type":"application/json; charset=utf-8"
//         },
//         body: JSON.stringify({
//             path: filePath
//         })
//     });
//     const files = await res.json();
//     console.log(files);

//     let html = '';
//     for (let file of files){
//         html += `
//         <div class="directory"> 
//                 <i class="fa fa-folder-open"></i>
//                 ${file} 
//         </div>`
//     }
//     document.querySelector("#file-container").innerHTML = html;
//     let directories = document.querySelectorAll('.directory');
//     for (let directory of directories){
//         directory.addEventListener('click', (event)=>{
//             let index = event.target.innerHTML.indexOf("i>")
//             let filePath = event.target.innerHTML.slice(index+2).trim();
//             loadFile(filePath);
//         })
// }
// }
// loadFile();