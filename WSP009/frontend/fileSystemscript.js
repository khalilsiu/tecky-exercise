

async function loadInfo(infos, id){
    // const res = await fetch('/directories');
    console.log(typeof(infos));
    let html = '';
    if (typeof(infos) === 'string'){
        html += `<div class=directories> ${infos} </div>`
    }
    else{
        for (let info of infos){
            html += `<div class=directories> ${info} </div>`
        }

    }
    document.querySelector(id).innerHTML = html;
}

// the html tag of #dir_path is selected and the response from the value is passed onto res which is then passed
// onto the loadDir() function which displays the content within
document.querySelector("#show_dir").addEventListener('submit', async (event)=>{
    event.preventDefault();
    const input_field = document.querySelector('#dir_path');
    const res = await fetch(`/directories?path=${input_field.value}`,{
        method: 'GET',
        headers:{
            "Content-Type":"application/json; charset=utf-8"
        }
        
    })

    const dirs = await res.json();
    const id = '#directories';

    loadInfo(dirs,id);
})

// the html id is selected with eventlistener added, form submission is prevented
// the whole form is stored as formData which allows it to be easily sent using fetch 
// is then passed to the route in the FileRouter to store the data
document.querySelector('#file_upload').addEventListener('submit', async (event)=>{
    event.preventDefault();
    const form = document.querySelector('#file_upload');
    const formData = new FormData(form);
    console.log(formData)

    await fetch('/upload',{
        method:'POST',
        body: formData
    })
    const uploadStatus = "Upload Success";
    const id = "#upload_status";
    loadInfo(uploadStatus, id);

})


var myImage = document.querySelector('.my-image');
fetch('https://upload.wikimedia.org/wikipedia/commons/7/77/Delete_key1.jpg')
    .then(res => res.blob())
    .then(res => {
        var objectURL = URL.createObjectURL(res);
        myImage.src = objectURL;
});