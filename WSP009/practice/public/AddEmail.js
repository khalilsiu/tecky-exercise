class AddEmail{
    
    constructor(){
        this.form = document.querySelector('#email_form');
        this.form.addEventListener('submit', this.onSubmit);
    }

    onSubmit = async(event)=>{
        event.preventDefault();
        const email = document.querySelector('#email');
        const password = document.querySelector('#password');
        // console.log(email.value);
        // console.log(password.value);
        

        let res = await fetch ('/email',{
            method :'POST',
            headers : {
                'Content-Type': 'application/json'
              },
            body : JSON.stringify({
                email: email.value,
                password: password.value
            })
        
        });
        this.form.reset();
        res.json();
        window.listEmail.listEmail();
    }
}