class ListEmail{
    constructor(){
        this.listEmail();
        this.list = document.querySelector('#email_list');
        this.registrants = [];
        window.socket.on('email-updated', this.listEmail);
    }
    listEmail = async ()=>{
        let res = await fetch('/email');
        this.registrants = await res.json();
        let items = ''

        for (let i = 0; i < this.registrants.length; i++){
            // the id you assign here has to be the respective id when you assigned to the individual entry, otherwise, you wouldnt be able to call
            items += `
            <li class= list-group-item> 
                <button class="btn btn-danger" data-id=${this.registrants[i].id}>
                    <i class="fa fa-times"></i> 
                </button>
                <button class="btn btn-info" data-id=${this.registrants[i].id}>
                    <i class="fa fa-pencil-ruler"></i>
                </button>
                ${this.registrants[i].email}
            </li>`
    
        }
        this.list.innerHTML = items;
        let crossButtons = document.querySelectorAll('.btn-danger');
        for (let button of crossButtons){
            button.addEventListener('click', (event)=>{
                window.deleteEmail.deleteEmail(event.currentTarget.dataset.id)
                this.listEmail();
            })
        }
        let editButtons = document.querySelectorAll('.btn-info');
        for (let button of editButtons){
            button.addEventListener('click', (event)=>{
                window.editEmail.populateEmail(this.registrants.find(x=>x.id == event.currentTarget.dataset.id))
            })
        }
    }
}