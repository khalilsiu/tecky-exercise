import * as express from 'express';
import bodyParser = require('body-parser');
import * as http from 'http';
import * as socketIO from 'socket.io';
import * as expressSession from 'express-session';

const app = express();
const server = new http.Server(app);
const io = socketIO(server);



app.use(express.static(__dirname+"/public"));
app.use(bodyParser.json())

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true,
    cookie:{secure:false}
}))


let emails:{id: number, email:string, password:string}[] = []
let id = 0;

app.get('/email', (req,res)=>{
    if (req.query.id){
        let response = emails.filter(x=> x.id == req.query.id)
        res.json(response)
    }
    else{
        res.json(emails);

    }
})

app.post('/email', (req,res)=>{

    emails.push ({
        id: id++,
        email: req.body.email,
        password: req.body.password
    });


    res.json({"result":"ok"});
    io.emit('email-updated')
});

app.delete('/email/:id',(req, res)=>{
    console.log(req.params.id)
    // use != since that one might be an int the other a string
    emails = emails.filter(x=>x.id != req.params.id);

    res.json({"result":"ok"});
    io.emit('email-updated')
})

app.put('/email/:id',(req,res)=>{
    // find method allows you to find and even change the values of it in the object
    let registrant = emails.find(x=>x.id == req.params.id);
    if (registrant){
        registrant.email = req.body.email
        registrant.password = req.body.password;

    }
    res.json({"result":"ok"})
    io.emit('email-updated')

})


const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});