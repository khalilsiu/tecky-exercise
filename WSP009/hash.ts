import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10

// 3 requirement
// one source string should be transformed to one hash string only
// one hash string should be transformed from one source string only, otherwise collision
// no easy way to derive the source string from the hash string

export async function hashPassword(plainPassword: string){
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

export async function checkPassword(plainPassword: string, hashPassword: string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

(async ()=>{
    console.log(await hashPassword("1234"))

})()