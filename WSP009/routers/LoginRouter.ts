import * as express from 'express';
import {Request, Response} from 'express';
import { LoginService } from '../services/LoginService';



export class LoginRouter{

    constructor(private loginService: LoginService){
        
    }
    public router(){
        const router = express.Router();
        router.post('/register',this.register);
        return router;    }

    private register  = async(req: Request,res:Response)=>{
        let {username, password} = req.body
        res.json( await this.loginService.register(username,password))
        
    }

}