import * as express from "express";
import { Request, Response } from "express";
import { FileService } from "../services/FilesService";
import * as multer from "multer";

export class FileRouter {
  private fileService: FileService;

  constructor() {
    this.fileService = new FileService();
  }

  public router() {
    const storage = multer.diskStorage({
      destination: function(req, file, cb) {
        // the file uploaded will be searching through the app.ts's directory's files
        cb(null, `temp/uploads`);
      },
      filename: function(req, file, cb) {
        cb(
          null,
          `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
      }
    });
    // we initialize multer as upload and is able to configure the features of the file
    // we
    const upload = multer({ storage });
    const router = express.Router();
    router.post("/upload", upload.single("pic_name"), this.upload);
    router.post("/files", this.files);
    router.get("/file", this.file);
    router.get("/delete", this.delete);
    
    return router;
  }

  files = async (req: Request, res:Response)=>{
    try{
        if (req.body.path){
            // disruptive operator: it explodes req.query to use the path as a variable directly
            let {path} = req.body;
            console.log(path);
            res.json(await this.fileService.listAllFiles(path));  
        }
        else{
            res.json(await this.fileService.listAllFiles());
        }
    }
    catch(err){
        res.status(404).send(err);
    }
  }
  private file = async (req: Request, res: Response) => {
    try {
      if (req.query.path) {
        let { path } = req.query;
        // res.download only available for the files within the current directory
        // it only prints out
        console.log(path)
        res.download(await this.fileService.downloadFile(path));
      }
    } catch (err) {
      return err;
    }
  };

  // the file in the request(uploaded will be responsed as a json)
  private upload = (req: Request, res: Response) => {
    // console.log("this is run");
    
    res.json(req.file);
  };

  private delete = async (req: Request, res: Response) => {
    if (req.query.path) {
      let { path } = req.query;
      let success = await this.fileService.deleteFile(path);
      console.log(success);
      if (success) {
        res.send("success");
      } else {
        res.send("failed");
      }
    } else {
      res.send("failed");
    }
  };


}
