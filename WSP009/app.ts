import * as express from 'express';
import * as expressSession from 'express-session';
import * as bodyParser from 'body-parser';
import { DirectoryRouter } from './routers/DirectoryRouter';
import { FileRouter } from './routers/FilesRouter';
import * as passport from 'passport';
import {isLoggedIn, loginFlow} from './guards'
import { LoginRouter } from './routers/LoginRouter';
import * as Knex from 'knex';
import {LoginService} from './services/LoginService'

const KnexConfig = require('./knexfile');
const knex = Knex(KnexConfig.development);




 
const app = express();


const dirRouter = new DirectoryRouter();
const fileRouter = new FileRouter();
export const loginService = new LoginService(knex);
const loginRouter = new LoginRouter(loginService);




app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'File listing app',
    resave: true,
    saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session()) // session is not mandatory, optional for passport to work
// user credentials is only passed once to the server during authentication. after authentication, only cookie will be passed as identifier for the user.
// Cookie is going to link to a session which only contains the id of the user

import './passport'
// passport has to come after initializing and session


// local strategy
app.post('/login',(...rest)=> passport.authenticate('local',loginFlow(...rest))(...rest))

// google strategy
app.get('/auth/google',passport.authenticate('google',{
    scope:['email','profile']}))
app.get('/auth/google/callback',
    (...rest)=>{
        const authFunction = passport.authenticate('google',loginFlow(...rest))
        authFunction(...rest)
    })
    // this will pass the error to our custom handler
    // this will be the real form of passing the param into a function
// 2 routes, one goes out to google, the other redirects back


app.use('/', loginRouter.router())
// the html POST method is passed through this route and through passport.ts to go through the local strategy, serialize deserialize. 
// if credential matches, will go into loginFlow in guard.ts (different files, because it is the structure of passport, local refers to local strategy)
// will redirect to urls with the error messages
// ...rest is just short form for response, request, next
//local is checked once at login, serialize and deserialize are checked everytime you visit
app.use(express.static(__dirname+'/public'))
// the file that contains login page shall not be guarded and needs to be put before the guarded ones
// otherwise, will create a redirect loop
app.use(isLoggedIn, express.static(__dirname+'/frontend'))
app.use('/', isLoggedIn,dirRouter.router());
app.use('/',isLoggedIn, fileRouter.router());
app.get('/logout',(req,res)=>{
    req.logOut();
    res.redirect('/')
})


const PORT = 8080;
app.listen(PORT, ()=>console.log(`Listening at http://localhost:${PORT}`));