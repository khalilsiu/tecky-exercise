import {Request,Response,NextFunction} from 'express';

interface User{
    username:string;
    password: string
}
export function isLoggedIn(req:Request, res:Response, next: NextFunction){

    if(req.user){
        next();
    }else{
        res.redirect('/login.html')
    }
}

export function loginFlow(req:Request, res:Response, next: NextFunction){
    // this is a custom function that we set to handle the error, to redirect to the path that we want
    // it can show a relevant css
    // we can set it to google logins as well
    return (err: Error, user: User, info:{message:string})=>{
        // the return value from LocalStrategy's done
        if (err){
            res.redirect('/login.html?error='+err.message)
        }else if(info && info.message){
            res.redirect('/login.html?error='+info.message);
        }else{
            req.logIn(user,(err)=>{
                // logIn goes into serializeUser function
                if(err){
                    res.redirect("/login.html?error="+"Failed to Login")
                }else{
                    console.log("loginflowing")
                    res.redirect("/")
                    // goes to deserialize user using id to get his data
                }
            })
        }
    }
}