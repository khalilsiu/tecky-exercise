import * as Knex from 'knex';
import {LoginService} from './LoginService';

const KnexConfig = require('../knexfile');
const knex = Knex(KnexConfig.testing);

describe('LoginService',()=>{

    let loginService: LoginService;
    beforeAll(()=>{
        loginService = new LoginService(knex);
    })

    it('should show isDefined when logging in a new user',async ()=>{
        await loginService.register('jamie','jamie')
        let found = await knex.select('*').from('users').where('username','jamie').andWhere('password','password').returning('id');
        expect(found).toBeDefined();
    })

    afterAll(()=>{
        knex.destroy();
    })
})
