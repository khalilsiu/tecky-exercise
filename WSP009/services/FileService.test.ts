import {FileService} from './FilesService'
import * as path from 'path'


describe("FileService",()=>{
    let fileService : FileService;

    beforeEach(()=>{
        fileService = new FileService();
    })
    it("should list all files",async()=>{
        let result = await fileService.listAllFiles(path.join(__dirname,"test"))
        expect(result).toEqual(["test.txt"])
    })

    it("should return true if file exists",async ()=>{
        let exists = await fileService.fileExists("tecky-exercise.md");
        expect(exists).toBe(true)
    })
})