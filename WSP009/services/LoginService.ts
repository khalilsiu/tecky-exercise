import * as fs from 'fs';
// import {hashPassword} from '../hash'
import * as Knex from 'knex';

interface User{
    email: string;
    username:string;
    password: string
}

export class LoginService{
    private userRecords: User[]
    constructor(private knex: Knex){
        if (!fs.readFileSync('report.json')){
            this.userRecords = []
        }
        else{
            this.userRecords = JSON.parse(fs.readFileSync('report.json','utf-8'));
        }
    }

    public async register(username:string, password: string){
        let user = await this.knex.select('username').from('users').where({username:username})
        console.log("hi",user);
        if (!user[0]){
             await this.knex('users').insert({username: username, password: password})
             return {result:"register success"};
        }
        else{
            return {result:"username taken"}
        }
    }

    // public login(username:string, password: string){
    //     console.log("login is called");
    //     if(this.userRecords.find(x=>x.username === username) && this.userRecords.find(x=>x.password === password)){
    //         return true;
    //     }
    //     else{
    //         return false;
    //     }
    // }

    public getUsers = async ()=>{
        this.userRecords = JSON.parse(await fs.promises.readFile('report.json','utf-8'))
        return this.userRecords;
    }

    // public registerUser = async (username:string, password:string, email:string) =>{
    //     this.userRecords.push({
    //         email: email,
    //         username:username,
    //         password: await hashPassword(password)
    //     })
    //     await fs.promises.writeFile('report.json',JSON.stringify(this.userRecords),{flag:"w"});
    // }


    
}   