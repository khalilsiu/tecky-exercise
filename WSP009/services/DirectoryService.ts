import * as fs from "fs";
import * as path from "path";

export class DirectoryService {
  private ROOT: string;
  constructor() {
    this.ROOT = path.join(__dirname, "..", "..");
  }
  async listAllDir(dirPath = this.ROOT) {
    try {
      let list: string[] = [];

        console.log("dirPath",dirPath);

      const dirNames: string[] = await fs.promises.readdir(dirPath);

      for (let dirName of dirNames) {
        let stat = await fs.promises.stat(path.join(dirPath, dirName));
        if (stat.isDirectory()) {
          list.push(dirName);
        }
      }
      return list;
    } catch (err) {
      let list = [];
      list.push({ result: "path not found" });
      return list;
    }
  }

  async dirExist(dirPath: any) {
    dirPath = await this.toFullPath(dirPath);
    let dirList = await this.listAllDir(this.ROOT);
    return dirList.includes(dirPath);
  }

  async toFullPath(dirPath: any) {
    return (dirPath = path.join(this.ROOT, dirPath));
  }

  async createDir(dirPath: any) {
    try {
      if (await this.dirExist(dirPath)) {
        throw new Error();
      } else {
        dirPath = await this.toFullPath(dirPath);
        await fs.promises.mkdir(dirPath);
        return { result: "directory created" };
      }
    } catch (err) {
      return { result: "directory failed to create" };
    }
  }
  async deleteDir(dirPath: any) {
    try {
      dirPath = path.join(this.ROOT, dirPath);
      await fs.promises.rmdir(dirPath);
      return { result: "directory deleted" };
    } catch (err) {
      return { result: "directory cannot be deleted" };
    }
  }

}
