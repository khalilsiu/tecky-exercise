import * as fs from 'fs';
import * as path from 'path';


export class FileService{
    private ROOT: string;
    constructor(){
        this.ROOT = path.join(__dirname, "..","..")
    }
    async listAllFiles(dirPath=this.ROOT) {
        try {
            let list:string[] = [];
            const dirNames: string[] = await fs.promises.readdir(dirPath);
            for (let dirName of dirNames) {
                let stat = await fs.promises.stat(path.join(dirPath,dirName));
                if (stat.isFile()) {
                    list.push(dirName);
                }
            }
            return list;
        }
        catch (err) {
            let list = [];
            list.push({"result":"path not found"})
            return list;
        }
    }

    public async toFullPath (filePath: any){
        try{
            // throw new Error();
            return filePath = path.join(this.ROOT, filePath);
        }
        catch(err){
            return {"result":"path cannot be converted to full"}
        }
    }

    public async fileExists(filePath: any){
        try{
            filePath = await this.toFullPath(filePath);
            console.log(this.ROOT);
            let fileList = await this.listAllFiles(this.ROOT);
            console.log(fileList.includes(filePath));
            return (fileList.includes(filePath));
        }
        catch(err){
            return {"result":"file existence is unable to identified"}
        }
    }

    public async downloadFile(filePath: any){
        try{
            if (this.fileExists(filePath)){
                return await this.toFullPath(filePath);
            }
            else{
                return {"result":"destination does not exist"};
            }
        }
        catch(err){
            return err;
        }
    }

    public async deleteFile(filePath: any){
        console.log(await this.fileExists(filePath))
        if (await this.fileExists(filePath)){
            filePath = await this.toFullPath(filePath);
            await fs.promises.unlink(filePath);
            return true;
        }
        else{
            return false;
        }
    }
    

}

