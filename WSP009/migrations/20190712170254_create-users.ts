import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('users');
    if (!hasTable){
        return knex.schema.createTable('users',(table)=>{
            table.increments();
            table.string('username');
            table.string('email');
            table.string('password')
        })
    }
    else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists('users');
}

