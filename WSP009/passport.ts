import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import * as passportOAuth2 from 'passport-oauth2'
import {checkPassword} from './hash'
import * as dotenv from 'dotenv';
// the google client id and client secret that do not want to release
import fetch from 'cross-fetch';
// fetch is front end stuff, need cross-fetch for backend

import {loginService} from './app'

dotenv.config();
const {GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET} = process.env;
// has to be run then they will stored in process.env

interface User{
    username:string;
    email:string;
    password: string
}

const LocalStrategy = passportLocal.Strategy;
const OAuth2Strategy = passportOAuth2.Strategy;

passport.use(new LocalStrategy(
    async function(username, password, done) {
        const users = await loginService.getUsers();
        console.log("username",username)
        const user = users.find((user:User)=>user.username == username);
        // console.log("user", user)
        if (!user){
            return done(null, false,{message:'Incorrect username!'})
        }
        const match = await checkPassword(password, user.password);
        if (match){
            console.log("match",match)
            return done(null, user);
        }
        else{
            return done(null, false, {message:'Incorrect password'})
        }

    }
))
passport.serializeUser(function(user: User,done){
    console.log("serializing")
    done(null, user.username) // put into passport object stored inside session
    // stores the least amount of identification info of the user
});

passport.deserializeUser(async function(username, done){
    // get back the user's data using the id
    // it would load once even for static stuff 
    console.log("deserializing")
    const users = await loginService.getUsers();
    const user = users.find((user:User)=>username == user.username)  
    if (user){
        done(null,user) // write from the passport object to the req.user for us to easily validate
    }
    else{
        done(new Error("User not Found"))
    }
})

passport.use('google', new OAuth2Strategy({
    authorizationURL: 'https://accounts.google.com/o/oauth2/auth',
    // need to tell the server where to go to get authorization and token
    tokenURL:"https://accounts.google.com/o/oauth2/token",
    clientID: GOOGLE_CLIENT_ID ? GOOGLE_CLIENT_ID :"",
    clientSecret: GOOGLE_CLIENT_SECRET? GOOGLE_CLIENT_SECRET: "",
    callbackURL: "http://localhost:8080/auth/google/callback"
    // which path to come back, this will connect to the app.ts route
},
async function(accessToken:string, refreshToken:string, profile:any, done:Function){
    // usually refreshToken and profile have nothing
    // not only do we want to check if the user is here, we also want to create user if not exist
    // can use postman to shoot the authorization key, relevant to the user, to grab the profile and email
     const res = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
         method: 'GET',
         headers:{
             "Authorization":`Bearer ${accessToken}`
         }
     });
     const result = await res.json();
     const users = await loginService.getUsers();
     const user = users.find((user)=>user.email == result.email)

     if (user){
        done(null, user)
     }else{
         done(new Error("User not exist!"))
     }
  }))

