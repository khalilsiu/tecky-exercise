import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    await knex('users').del();
    const [hannahp, pepe] = await knex('users').insert([
        {
          "username": "hannahp",
          "email":"siu.anthony.dbs@gmail.com",
          "password": "$2b$10$xz53tfDnEBddfD3kBJZ6Ne8DmDmR7v2WBA8IitUoO7NBQabewM5g."
        },
        {
          "username": "pepe",
          "email":"siu.anthony.cl@gmail.com",
          "password": "$2b$10$TB.LlN.qNayrCvWJkoB4y.w.e1ZKcXWe2WrFDLIHpSUH6A8buYb5a"
        }
      ]).returning('id');

      console.log(hannahp, pepe);
};
