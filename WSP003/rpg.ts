import * as readline from 'readline';


const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

type playerType = Amazon|Paladin|Barbarian|Sorcerer|Necromancer;


class Game {
    private player: playerType;
    // private monster: Monster;
    constructor() {

    }

    start() {
        const readCommand = () => {
            readLineInterface.question("Please enter your name: ", async (answer: string) => {
                if (!answer) {
                    readCommand();
                }
                else {
                    await (readCommand2(answer));
                }

            })
        }
        readCommand();
        const readCommand2 = (answer: string) => {
            readLineInterface.question("Choose your occupation: Amazon, Paladin, Barbarian, Sorcerer, Necromancer", async (reply: string) => {
                reply = reply.toLowerCase();
                if (reply === "amazon" || reply === "paladin" || reply === "barbarian" || reply === "sorcerer" || reply === "necromancer") {
                    console.log(`You chose to be a ${reply} `)
                    switch (reply) {
                        case "amazon": this.player = new Amazon(answer);
                        break;
                        case "paladin": this.player = new Paladin(answer);
                        break;
                        case "barbarian": this.player = new Barbarian(answer);
                        break;
                        case "sorcerer": this.player = new Sorcerer(answer);
                        break;
                        case "necromancer": this.player = new Necromancer(answer);
                        break;
                }
                await (this.runGame());
                this.newGame();
            }
                else {
            readCommand2(answer);
        }

    })
}
    }

newGame(){
    const readCommand3 = () => {
        readLineInterface.question("New game? Y/N: ", async (answer: string) => {
            answer = answer.toLowerCase();
            if(answer === "Y") {
                await this.start();
            }
            else if (answer === "N"){
                process.exit(-1);
            }
            
            else {
                readCommand3();
            }
        })
    }
    readCommand3();
}

runGame(){

    let monsterArray = Array(3).fill(null).map(x => this.createMonster());


    for (let monster of monsterArray) {
        console.log(`${this.player.getName()} encounters a ${monster.getHp()} monster (HP: ${monster.getHp()})`);
        while (monster.isAlive()) {
            let isCritical: boolean = Math.random() > 0.8;
            let attackType = Math.random() > 0.5;
            attackType ? this.player.primaryAttack(monster, isCritical) : this.player.secondaryAttack(monster, isCritical);
            if (monster.getHp() === 0) {
                console.log(`The ${monster.getLevel()} monster is defeated (HP: ${monster.getHp()})`, isCritical ? `[CRITICAL]` : ``);
                if (monster.getLevel() === "RARE") {
                    this.player.gainExperience(20);
                }
            }
            else {
                console.log(`${this.player.getName()} attacked the ${monster.getLevel()} monster (HP: ${monster.getHp()})`, isCritical ? `[CRITICAL]` : ``);

            }
        }

    }

}

createMonster(): Monster{
    const MONSTER_LEVELS = [normalMonster, rareMonster, bossMonster];
    return new MONSTER_LEVELS[Math.floor(Math.random() * 3)];
}
    // 


}

interface Weapon {
    attack(monster: Monster, playerStrength: number, critical: number): void;
}

class Arrow implements Weapon{
    private weaponPower: number;
    constructor(){
        this.weaponPower = 0.1;
    }
    attack(monster: Monster, playerStrength: number, critical: number): void{
        let attackPower:number = playerStrength * this.weaponPower*critical;
        console.log(`Attack is Arrow! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}

class Melee implements Weapon {
    private weaponPower: number;
    constructor() {
        this.weaponPower = 0.2;

    }
    attack(monster: Monster, playerStrength: number, critical: number): void {
        let attackPower: number = playerStrength * this.weaponPower * critical;
        console.log(`Attack is melee! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}

class Spear implements Weapon {
    private weaponPower: number;
    constructor() {
        this.weaponPower = 0.4;

    }
    attack(monster: Monster, playerStrength: number, critical: number): void {
        let attackPower: number = playerStrength * this.weaponPower * critical;
        console.log(`Attack is spear! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}

class Magic implements Weapon{
    private weaponPower: number;
    constructor(){
        this.weaponPower = 0.6;

    }
    attack(monster: Monster, playerStrength: number, critical: number): void{
        let attackPower:number = playerStrength * this.weaponPower*critical;
        console.log(`Attack is magic! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}

class Summoning implements Weapon{
    private weaponPower: number;
    constructor(){
        this.weaponPower = 0.8;

    }
    attack(monster: Monster, playerStrength: number, critical: number): void{
        let attackPower:number = playerStrength * this.weaponPower*critical;
        console.log(`Attack is monster summoning! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}


class Minions implements Weapon{
    private weaponPower: number;
    constructor(){
        this.weaponPower = 0.5;

    }
    attack(monster: Monster, playerStrength: number, critical: number): void{
        let attackPower:number = playerStrength * this.weaponPower*critical;
        console.log(`Attack is minions attack! Power is ${attackPower}`);
        monster.injure(attackPower);
    }
}


interface Player {
    primaryAttack(monster: Monster, isCritical: boolean): void;
    secondaryAttack(monster: Monster, isCritical: boolean): void;
    gainExperience(exp: number): void;
}

class Barbarian implements Player {
    private strength: number;
    private name: string;
    private primaryWeapon: Weapon;
    private secondaryWeapon: Weapon;
    constructor(name: string){
        this.strength = 90;
        this.name = name;
        this.primaryWeapon = new Melee();
        this.secondaryWeapon = new Arrow();

    }

    primaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.primaryWeapon.attack(monster, this.strength, critical);


    }

    secondaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.secondaryWeapon.attack(monster, this.strength, critical);


    }

    gainExperience(exp: number){
        console.log(`${this.getName()} has gained ${exp} strength`);
        this.strength += exp;
    }

    getName(): string{
        let name = this.name[0].toUpperCase() + this.name.slice(1,this.name.length);
        return name;
    }
}


class Amazon implements Player {
    private strength: number;
    private name: string;
    private primaryWeapon: Weapon;
    private secondaryWeapon: Weapon;
    constructor(name: string){
        this.strength = 80;
        this.name = name;
        this.primaryWeapon = new Arrow();
        this.secondaryWeapon = new Spear();

    }

    primaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.primaryWeapon.attack(monster, this.strength, critical);


    }
  
    secondaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.secondaryWeapon.attack(monster, this.strength, critical);


    }

    gainExperience(exp: number){
        console.log(`${this.getName()} has gained ${exp} strength`);
        this.strength += exp;
    }

    getName(): string{
        let name = this.name[0].toUpperCase() + this.name.slice(1,this.name.length);
        return name;
    }
}

class Paladin implements Player {
    private strength: number;
    private name: string;
    private primaryWeapon: Weapon;
    private secondaryWeapon: Weapon;
    constructor(name: string) {
        this.strength = 80;
        this.name = name;
        this.primaryWeapon = new Melee();
        this.secondaryWeapon = new Spear();

    }

    primaryAttack(monster: Monster, isCritical: boolean) {
        let critical = 1;
        isCritical ? critical = 2 : critical = 1;
        this.primaryWeapon.attack(monster, this.strength, critical);


    }

    secondaryAttack(monster: Monster, isCritical: boolean) {
        let critical = 1;
        isCritical ? critical = 2 : critical = 1;
        this.secondaryWeapon.attack(monster, this.strength, critical);


    }

    gainExperience(exp: number) {
        console.log(`${this.getName()} has gained ${exp} strength`);
        this.strength += exp;
    }

    getName(): string {
        let name = this.name[0].toUpperCase() + this.name.slice(1, this.name.length);
        return name;
    }
}

class Sorcerer implements Player {
    private strength: number;
    private name: string;
    private primaryWeapon: Weapon;
    private secondaryWeapon: Weapon;
    constructor(name: string){
        this.strength = 50;
        this.name = name;
        this.primaryWeapon = new Magic();
        this.secondaryWeapon = new Summoning();

    }

    primaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.primaryWeapon.attack(monster, this.strength, critical);


    }

    secondaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.secondaryWeapon.attack(monster, this.strength, critical);


    }

    gainExperience(exp: number){
        console.log(`${this.getName()} has gained ${exp} strength`);
        this.strength += exp;
    }

    getName(): string{
        let name = this.name[0].toUpperCase() + this.name.slice(1,this.name.length);
        return name;
    }
}

class Necromancer implements Player {
    private strength: number;
    private name: string;
    private primaryWeapon: Weapon;
    private secondaryWeapon: Weapon;
    constructor(name: string){
        this.strength = 80;
        this.name = name;
        this.primaryWeapon = new Magic();
        this.secondaryWeapon = new Minions();

    }

    primaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.primaryWeapon.attack(monster, this.strength, critical);


    }

    secondaryAttack(monster: Monster, isCritical:boolean){
        let critical = 1;
        isCritical ? critical = 2: critical = 1;
        this.secondaryWeapon.attack(monster, this.strength, critical);


    }

    gainExperience(exp: number){
        console.log(`${this.getName()} has gained ${exp} strength`);
        this.strength += exp;
    }

    getName(): string{
        let name = this.name[0].toUpperCase() + this.name.slice(1,this.name.length);
        return name;
    }
}

// interface by definition is public, therefore, its best to achieve everything with methods
interface Monster {
    getHp(): number;
    // ? is specified if this is an optional parameter
    injure(damage: number): void;
    isAlive(): boolean;
    getLevel(): string;
}
class normalMonster implements Monster {
    protected hp: number;
    constructor() {
        this.hp = ((Math.floor(Math.random() * (3 - 1)) + 1) * 100);
    }

    getHp() {
        return this.hp;
    }

    injure(damage: number) {
        this.hp -= damage;
        this.hp = Math.max(0, this.hp);
    }

    isAlive(): boolean {
        return this.hp > 0;
    }
    getLevel(): string {
        return "NORMAL";
    }
}

class rareMonster extends normalMonster implements Monster {
    // you have to correspond to the above definition as the interface
    // injure(damage: number, playerObject: Player){
    //     super.injure(damage, playerObject);
    //     if (this.hp === 0){
    //         playerObject.gainExperience(20);
    //     }

    // }
    getLevel(): string {
        return "RARE"
    }
}

class bossMonster extends normalMonster implements Monster {
    private resistance: number;
    private fullHp: number;
    constructor() {
        super();
        this.fullHp = this.hp;
        this.resistance = 1;

    }

    injure(damage: number) {
        if (this.hp < this.fullHp * 0.5) {
            this.resistance = 2;
        }
        this.hp -= damage / this.resistance;
        this.hp = Math.max(0, this.hp);

    }

    getLevel(): string {
        return "BOSS";
    }
}

let newGame = new Game();
newGame.start();