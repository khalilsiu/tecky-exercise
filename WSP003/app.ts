

class BankAccount{

    private balance: number;

    constructor(){
        this.balance = 0;
    }

    deposit(amount: number){
        this.balance += amount;
    }

    withdraw(amount:number){
        if (this.balance < amount){
            console.log(this.balance - amount);
            throw new Error("Not enough balance!");
        }
        this.balance -= amount;
    }
}

const account = new BankAccount();
account.deposit(100);
account.withdraw(110);
