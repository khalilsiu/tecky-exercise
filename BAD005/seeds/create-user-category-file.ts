import * as Knex from "knex";

export async function seed(knex: Knex){
    // Deletes ALL existing entries
    await knex('users').del();
    await knex('category').del();
    await knex('file').del();

    let [ user ] =await knex.insert({
        username: 'admin',
        password: 'admin',
        level: 'admin'
    }).into('users')
      .returning('id');

    let [ category ] = await knex.insert({
        name: 'useful'
    }).into('category')
    .returning('id')

    console.log(user, category)

    return await knex.insert({
        name: 'hac.xls',
        content: 'hahah',
        isfile: true,
        category_id: category,
        user_id: user
    }).into('file');
};
