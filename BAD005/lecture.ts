import * as Knex from 'knex';
const KnexConfig = require('./knexfile')
const knex = Knex(KnexConfig.development)

const main = async ()=>{
    // students.* will restrict the data to only show the table of students but not teachers
    // const innerJoin = await knex.select('students.*').from('students')
    // .innerJoin('teachers','students.teacher_id', 'teachers.id')
    // console.log(innerJoin)

    // const leftOuterJoin = await knex.select("students.*").from('students')
    // .leftOuterJoin('teachers','students.teacher_id','teachers.id')
    // console.log(leftOuterJoin)

    // you can order by the 2 conditions
    // let order = await knex.select('*').from('students')
    // .orderBy('level','desc')
    // .orderBy('date_of_birth','asc');

    // let limitOffset = await knex.select('*').from('students')
    // .orderBy('level')
    // .limit(2)
    // .offset(1);

    // let aggregate = await knex.select('teachers.name','students.name').count('students.id')
    // .from('teachers')
    // .innerJoin('students','teachers.id','students.teacher_id')
    // .groupBy('teachers.name','students.name')

    // to select 2 columns and group by them
    // let sum = await knex.select('teachers.name','students.name').sum('students.level')
    // .from('teachers')
    // .innerJoin('students','teachers.id','students.teacher_id')
    // .groupBy('teachers.name','students.name')

    // having has to be havingRaw
    
    let having = await knex.select('teachers.name').max('students.level')
    .from('teachers')
    .innerJoin('students','teachers.id','students.teacher_id')
    .groupBy('teachers.name')
    .havingRaw("max(students.level) > 25");

    console.log(having)
}

main();