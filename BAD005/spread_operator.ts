
let obj = {
    a: 1,
    b: 2,
    c: 3
}

// let {a, b, c} = obj // varibles a, b, c are assigned 1,2,3 respectively

// let {a, ...rest} =  obj // ... breaks open obj, assigns the first to a, and assign the rest to rest
// therefore, if you want to shorten an object, you can get rest.


let array = [1,2,3,4,5]

// let [g,h,i,j,k] = array // they are assigned respectively to the values

let [g, ...rest] = array // same as the above


let ids = [ { id: 17, username: 'admin' },
{ id: 18, username: 'alex' },
{ id: 19, username: 'gordon' },
{ id: 20, username: 'michael' } ]


// let haha = ids.reduce((acc, user)=>{
//     return {
//         // spread operator, since object cannot be pushed or whatsoever, you need to user ...
//         // ... breaks open the acc object, and put acc back in, to concentate the object
//         ...acc,
//         [user.username]: user.id
//     }
// },{})

let haha = ids.reduce((acc, user)=>{
    acc[user.username] = user.id
    return acc;
},{})

console.log(haha)

console.log(haha)