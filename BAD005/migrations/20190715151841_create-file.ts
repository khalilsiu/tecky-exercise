import * as Knex from "knex";


export async function up(knex: Knex){
    return await knex.schema.createTableIfNotExists('file',(table)=>{
        table.increments();
        table.string('name').notNullable();
        table.text('content');
        table.boolean('isfile').notNullable();
        table.timestamps(false, true);
        table.integer('category_id').unsigned();
        table.foreign('category_id').references('category.id');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
    })
}


export async function down(knex: Knex){
    return await knex.schema.dropTableIfExists('file');
}

