import * as Knex from "knex";


export async function up(knex: Knex){
    return await knex.schema.createTableIfNotExists('category',(table)=>{
        table.increments();
        table.string('name').notNullable();
        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return await knex.schema.dropTableIfExists('category');
}

