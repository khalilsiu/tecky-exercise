import * as Knex from "knex";


export async function up(knex: Knex){
    return await knex.schema.createTableIfNotExists('users',(table)=>{
        table.increments();
        table.string('username').notNullable();
        table.string('password').notNullable();
        table.string('level').notNullable();
        table.timestamps(false,true)
    })
}


export async function down(knex: Knex){
    return await knex.schema.dropTableIfExists('users');
}

