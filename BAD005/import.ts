import * as XLSX from "xlsx";
import * as Knex from "knex";

const KnexConfig = require("./knexfile");
export const knex = Knex(KnexConfig.development);
const workbook = XLSX.readFile("BAD005-exercise.xlsx");

interface User {
  Username: string;
  Password: string;
  Level: string;
}

interface Category {
  Name: string;
}

interface File {
  Name: string;
  Content: string;
  isFile: number;
  Category: string;
  Owner: string;
}
const sheet1 = workbook.Sheets["user"];
const sheet2 = workbook.Sheets["file"];
const sheet3 = workbook.Sheets["category"];

let users: User[] = XLSX.utils.sheet_to_json(sheet1);
let files: File[] = XLSX.utils.sheet_to_json(sheet2);
let categories: Category[] = XLSX.utils.sheet_to_json(sheet3);

// console.log(files);

async function importData() {
    knex.transaction(async(trx)=>{
        await trx("file").del();
        await trx("users").del();
        await trx("category").del();
        const usersToBeWritten = users.map(user => ({
          username: user.Username,
          password: user.Password,
          level: user.Level
        }));
      
        // the return value is an array of objects that has to be assigned to a variable first
        let user_ids = await trx
          .insert(usersToBeWritten)
          .into("users")
          .returning(["id", "username"]);
      
        // since we wish to create ONE object like that, therefore from an array we need to use REDUCE!
        // {
        //     admin: 1,
        //     alex: 2,
        //     peter: 3
        // }
        let user_reference = user_ids.reduce((acc, user) => {
          return {
            // spread operator, since object cannot be pushed or whatsoever, you need to user ...
            // ... breaks open the acc object, and put acc back in, to concatenate the object
            // and that, if you need to use a variable as key, you need to use []
            ...acc,
            [user.username]: user.id
          };
        }, {});
        // console.log(user_reference)
      
        const categoriesToBeWritten = categories.map(category => ({
          name: category.Name
        }));
      
        let category_ids = await trx
          .insert(categoriesToBeWritten)
          .into("category")
          .returning(["id", "name"]);
      
        let category_reference = category_ids.reduce((acc, category) => {
          return {
            ...acc,
            [category.name]: category.id
          };
        }, {});
      
        // console.log(category_reference)
      
        // the file's interface has to match its columns
        // you would need to find the user's id by mapping the user_reference and for categories as well
        const filesToBeWritten = files.map(file => ({
          name: file.Name,
          content: file.Content,
          isfile: file.isFile == 1,
          category_id: category_reference[file.Category],
          user_id: user_reference[file.Owner]
        }));
      
        await trx.insert(filesToBeWritten).into("file");

    })
}

importData();
