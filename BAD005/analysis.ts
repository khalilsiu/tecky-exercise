import * as Knex from "knex";

const KnexConfig = require("./knexfile");
export const knex = Knex(KnexConfig.development);

async function main(){
    let fileCount = await knex.select('users.username').count('file.id')
    .from('users')
    .innerJoin('file','users.id','file.user_id')
    .groupBy('users.username')

    console.log(fileCount);

    let categoryCount = await knex.select('category.name').count('file.id')
    .from('category')
    .innerJoin('file','category.id','file.category_id')
    .groupBy('category.name');

    console.log(categoryCount)


    //How many files of the category important does alex have ?
    //select users.username, category.name, count(file.id) from file 
    //inner join users on users.id = file.user_id 
    //inner join category on category.id = file.category_id 
    //where category.name = 'Important' and users.username = 'alex'
    //group by users.username, category.name 

    let alexImportant = await knex.select('users.username').select('category.name').count('file.id')
    .from('file')
    .innerJoin('users','users.id','file.user_id')
    .innerJoin('category','category.id','file.category_id')
    .where('category.name','Important')
    .andWhere('users.username','alex')
    .groupBy('users.username')
    .groupBy('category.name')

    console.log(alexImportant)

    let userCount = await knex.select('users.username').count('file.id')
    .from('file')
    .innerJoin('users','users.id','file.user_id')
    .groupBy('users.username')
    .havingRaw("count(file.id)>800")

    console.log(userCount); 
}
main()