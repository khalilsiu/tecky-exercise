

// import * as fs from 'fs';

// fs.readFile('quotes.txt', function(err:Error, data:Buffer){
//     if (err){
//         console.log(err);
//         return;
//     }
//     console.log(data);
// });

import * as path from 'path';
// __dirname is like pwd
console.log(path.join(__dirname,"/////quote.txt"));

// console.log("Step 1");
// const dijkstraQuote1 = "Computer science is no more about computers than astronomy is about telescopes.\n";
// console.log("Step 2");
// const dijkstraQuote2 = "Simplicity is prerequisite for reliability.\n";

// console.log("Step 3");
// // Flag w overwrites the original content and create the if it does not exist
// fs.writeFile('quotes-dijkstra.txt',dijkstraQuote1,{flag:'w'},function(err:Error){
//     console.log("Step 5");
//     if(err){
//         console.log(err);
//     }
//     console.log("Step 6");
//     // Flag a+ appends to the content and create the file if it does not exist
//     fs.writeFile('quotes-dijkstra.txt',dijkstraQuote2,{flag:'a+'},function(err:Error){
//         console.log("Step 7");
//         if(err){
//             console.log(err);
//         }
//         console.log("Step 8");
//     });
// });
// console.log("Step 4")

// console.log("This is the normal console.log");

// console.error("This is the logging facility for the ERROR level");

// console.warn("This is the logging facility for the WARN level")

// console.info("This is the logging facility for the INFO level");

// console.debug("This is the logging facility for the DEBUG level");

// import * as readline from 'readline';

// const readLineInterface = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
    
// })

// readLineInterface.question("What is your name?", (answer:string)=>{
//     console.log(`Your name is ${answer}`);
//     readLineInterface.close();
// });

// function power(x:number,y:number){
//     return x**y;
// }


// setImmediate(function(){
//     console.log(`setImmediate run power function = ${power(2,3)}`);
// })

// setTimeout(function(){
//     console.log(`setTimeout run power function = ${power(2,3)}`);
// },5000);

// import * as os from 'os';

// console.log(os.cpus());
// console.log(os.homedir());
// console.log(os.hostname());
// console.log(os.freemem());
// console.log(os.totalmem());

// import * as path from 'path';

// const folder1 = "folder1/";
// const folder2 = "/folder2/";
// const file1 = "file1";

// // Using path
// path.join("folder1/","/folder2/","file1");
// // -> folder1/folder2/file1

// // Using string concat
// folder1 + folder2 + file1
// // -> folder1//folder2/file