"use strict";
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
function listAllJsRecursive(filePath) {
    fs.readdir(filePath, function (err, files) {
        if (err) {
            var index = filePath.indexOf(".");
            if (filePath.slice(index) === ".js") {
                console.log(filePath);
            }
            return;
        }
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            var childPath = path.join(filePath, file);
            listAllJsRecursive(childPath);
        }
    });
}
listAllJsRecursive('/Users/anthonysiu/codes/tecky-exercises/');
