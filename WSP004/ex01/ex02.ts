
import * as fs from 'fs';
import * as path from 'path';


function listAllJsRecursive(filePath: string){
    fs.readdir(filePath, function(err:Error, files:string[]){
        if (err){
            console.log(filePath);


        }
        else{
            for (let file of files){
                let childPath = path.join(filePath,file);
                fs.stat(childPath, function(err:Error, stats:fs.Stats){
                    if (err){
                        console.log(err);
                    }
                    if (stats.isDirectory()){
                        listAllJsRecursive(childPath);
                    }
                    // do not need to use slice!
                    else if (stats.isFile() && childPath.endsWith(".js")){
                        console.log(childPath);
                    }
                });
            }

        }
    })
}

listAllJsRecursive(__dirname);