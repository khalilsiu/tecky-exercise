import * as tf from '@tensorflow/tfjs-node'

async function trainRegressionModel(){
    // sequential means the layers in the deep learning, one after one. RNN would be concurrent, good for word compilation
    const model = tf.sequential();
    // You have to specify the inputShape of the first layer
    model.add(tf.layers.dense({units: 3, inputShape: [1]}));
    model.add(tf.layers.dense({units: 4}));
    // This layer should have the same dimension with the output data
    model.add(tf.layers.dense({units: 1})); 
    model.compile({loss: 'meanSquaredError', optimizer: tf.train.sgd(0.001)});
    // Generate the summary of the network
    model.summary();

    // Generate some synthetic data for training.
    const xData = Array(20).fill(null).map((value,index)=>[index]);
    const yData = Array(20).fill(null).map((value,index)=>[index+index+1] );
    const xs = tf.tensor2d(xData, [xData.length, 1]);
    const ys = tf.tensor2d(yData, [yData.length, 1]);


    // Train the model using the data.
    await model.fit(xs, ys, {epochs: 1000})

    const testingData = Array(50).fill(null).map((value,index)=>[index+100]);
    // Use the model to do inference on a data point the model hasn't seen before:
    const result = model.predict(tf.tensor2d(testingData, [testingData.length, 1])) as tf.Tensor<tf.Rank> ;
    result.print(true);
}