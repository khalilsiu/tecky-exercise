
import * as Papa from 'papaparse';
import * as path from 'path'
import * as fs from 'fs';
import * as tf from '@tensorflow/tfjs-node';

// Change this one to load data from another folder
const DATA_FOLDER = path.join(__dirname,"mnist-in-csv");

interface DataFormat{
    data: Array<Float32Array>,
    label: Array<Float32Array>
}


function parse(filename:string){
    const readStream = fs.createReadStream(path.join(DATA_FOLDER,filename));
    return new Promise<DataFormat>((resolve,reject)=>{
        Papa.parse(readStream,{
            complete: async (results)=>{
                // in the data, the first row is unnecessary for coding, they are the headers 
                const rows = results.data.slice(1,results.data.length);
                const label = rows.map((row)=>{
                    const arr = Array(10).fill(0);
                    arr[row[0]] = 1;
                    // label is the first elem of the rows
                    return Float32Array.from(arr) //JS array supports different types, this changes it into only stores float32 >> faster
                })
                // data are the ones after
                const data = rows.map((row)=>Float32Array.from(row.slice(1,row.length)))

                // since the output is a multiclass classification: [0,0.1,0.2,0.7,0,0,0,0,0] >> possibilities of the results
                resolve ({
                    label,
                    data}
                )
               // Read the data and use resolve to return the content back the constructor
            }
        })
    });
}

parse('mnist_train.csv')



export class MnistDataset{

    private trainData: Float32Array[];
    private testData: Float32Array[];
    private trainLabel: Float32Array[];
    private testLabel: Float32Array[];

    constructor(){

    }
    async loadCSV(){
        let testCSV = await parse('mnist_test.csv');
        this.testData = testCSV.data;
        this.testLabel = testCSV.label;

        const trainCSV = await parse('mnist_train.csv');
        this.trainData = trainCSV.data;
        this.trainLabel = trainCSV.label;

    }

    getTrainDataset(){
       // TODO: Return the Train dataset using the `tf.tensor2d`
       const dataSize = this.trainData.length
       const labelSize = this.trainLabel.length
       return{
           // convert array into tensor, the data type that tf understands
            // shape is the number of training data as well as the features
            // has to revert back to ANY
           data: tf.tensor2d(this.trainData as any as Float32Array,[dataSize,784]),
           label: tf.tensor2d(this.trainLabel as any as Float32Array,[labelSize,10])
       }
    }

    getTestDataset(){
       // TODO: Return the Test Dataset using the `tf.tensor2d` 
       const dataSize = this.testData.length
       const labelSize = this.testLabel.length
       return{
           // convert array into tensor, the data type that tf understands
            // shape is the number of training data as well as the features
            // has to revert back to ANY
           data: tf.tensor2d(this.testData as any as Float32Array,[dataSize,784]),
           label: tf.tensor2d(this.testLabel as any as Float32Array,[labelSize,10])
       }
    }
}