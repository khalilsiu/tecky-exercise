import * as tf from '@tensorflow/tfjs-node';
import { Tensor, Rank } from '@tensorflow/tfjs-node';
import { MnistDataset } from './data';


async function run(learningRate = 0.01 , trainingEpochs = 80, batchSize = 100){
    const dataset = new MnistDataset();
    await dataset.loadCSV();


    const trainDataset = dataset.getTrainDataset();
    const trainData = trainDataset.data;
    const trainLabel = trainDataset.label;

    console.log(trainData)
    console.log(trainLabel)

    // TODO: Set up the Variable here

    // weights are randomized, not zero, so that they are able to changed accordingly
    // each layer of a neural network, it would pass through Y = Wx + b, to give an output then to an activation function
    // map 784 possibilities to 10 locations
    const weights = tf.randomUniform<Rank.R2>([784,10]).variable();
    const bias = tf.randomUniform<Rank.R1>([10]).variable()

    const predict = (x:Tensor<Rank.R2>)=>{
        // TOOD: Make use of the Variable to predict the result
        // Softmax is trying to make all inputs into a list of probabilities, since we only have one layer to output.
        // otherwise it would be going into an activation function
        const predictions = x.matMul(weights).add(bias).logSoftmax();
        return predictions
    }

    const losses = (batch:number)=>{
        // TODO: Use tensor.slice to get back the corresponding batch
        // need to shift the batch as it loops
        const startIndex = batch * batchSize // if batch is 10 , startIndex = 10 * 100 = 1000
        let trainDataBatch = trainData.slice(startIndex, batchSize)
        // TOOD: Calculate the loss using cross entropy
        // Use the probability to compare the predictions with the label
        // using cross entropy
        const losses = predict(trainDataBatch).mul(trainLabel.slice(startIndex,batchSize))
                            .sum(1)
                            .neg()
                            .mean();
        return losses as Tensor<Rank.R0>

    }

    // stochastic (random) gradient descent
    const optimizer = tf.train.sgd(learningRate);

    for(let epoch= 0; epoch< trainingEpochs; epoch++){
        // need to split as the data size is too large
        // number of rows: shape[0]
        const totalBatch = trainData.shape[0] / batchSize // 60000/100 = 600
        // optimize the loss function 
        let averageCost = 0;
        // every batch, it improves
        for(let batch = 0; batch < totalBatch; batch++){
            let loss = optimizer.minimize(()=>losses(batch),true)
            if (loss){
                // add on and get the average cost as each batch loops
                averageCost += (await loss.data())[0] / totalBatch
            }

        }
        // TODO: batch by batch , use the `optimizer.minimize` to minimize the loss function
        console.log(`Epoch = ${epoch}, Average Cost = ${averageCost}`)
    }


    const testDataset = dataset.getTestDataset();
    const testData = testDataset.data;
    const testLabel = testDataset.label;

    // since the label and data array are being casted into [0,0,0,0,0,1,0,0], by doing that, they are able to compare and check the accuracy
    const correctPrediction = predict(testData).argMax(1).equal(testLabel.argMax(1))
    const accuracy = await correctPrediction.cast('float32').mean().data()

    //  TODO: Test your Result Here and generate the accuracy to be printed out
    console.log(`Accuracy on testing data is ${accuracy}`);

}    

run();