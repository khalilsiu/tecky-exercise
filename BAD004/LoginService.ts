
import * as Knex from 'knex';
const KnexConfig = require('./knexfile');
const knex = Knex(KnexConfig[process.env.NODE_ENV || 'development'])


export class LoginService{

    constructor(private knex: Knex){
    }

    //create
    public async register(name:string, level: number, date_of_birth:string){
        let user = {name: name, level: level, date_of_birth:date_of_birth};
        let id = await this.knex('students').insert(user).returning('id');
        return id;
    }


    public getUsers = async ()=>{
        return this.knex.select('*').from('students');
    }
    public deleteUser = async (id: number)=>{
        return await this.knex('students').delete().where({id:id}).returning('id');
    }
    public updateUser = async (id: number, name:string, level: number, date_of_birth:string) =>{
        let user = {name: name, level: level, date_of_birth:date_of_birth};
        return await this.knex('students').update(user).where({id:id}).returning('id');
    }

    
}   

let service = new LoginService(knex);

// service.register("jamie",14, '1955-01-30').then(x=>console.log(x));
// service.getUsers().then(x=>console.log(x));
// service.deleteUser(2).then(x=>console.log(x))
service.updateUser(3, 'cercei', 28, '1993-02-04').then(x=>console.log(x))