import * as Knex from 'knex';

export class StudentService{
    constructor(private knex:Knex){

    }

    getStudents(){
        return this.knex.select("*").from('students');
    }

    addStudent(body:any){
        return this.knex.table("students").insert({
            name: body.name,
            level: body.level,
            date_of_birth: body.date_of_birth
        });
    }

    updateStudent(id:number,body:any){
        return this.knex.table("students")
                .update(body).where("id",id);
    }

    deleteStudent(id:number){
        return this.knex.table("students").where("id",id).delete();
    }

}