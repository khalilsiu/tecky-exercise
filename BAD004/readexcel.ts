import * as Knex from "knex";
import * as XLSX from "xlsx";

// must use require function

const KnexConfig = require("./knexfile");

const knex = Knex(KnexConfig.development);

// interface USER {
//   username: string;
//   password: string;
//   level: string;
// }

interface FILE {
    name: string;
    content: string;
    isFile: number;
}

let workbook = XLSX.readFile("BAD003-exercise.xlsx");

// let user_sheet = workbook.Sheets[workbook.SheetNames[0]];
let file_sheet = workbook.Sheets[workbook.SheetNames[1]]
// let category_sheet = workbook.Sheets[workbook.SheetNames[2]];
// let users: USER[] = XLSX.utils.sheet_to_json(user_sheet);
let files: FILE[] = XLSX.utils.sheet_to_json(file_sheet);
// let categories: {name:string}[] = XLSX.utils.sheet_to_json(category_sheet);

(async () => {
  await knex("user").del();
  console.log(files);
//   let user_id = await knex("user")
//     .insert(users)
//     .returning("id");
//   console.log(user_id);

  let file_id = await knex("file")
    .insert(files)
    .returning('id');
    console.log(file_id);

    // let category_id = await knex('category')
    //     .insert(categories)
    //     .returning('id')
    // console.log(category_id)
})();

// async function run() {
//     // let select = await knex.select('name','level','date_of_birth').from('teachers');
//     // let select2 = await knex('teachers').column(['name','level','date_of_birth'])
//     // let select3 = await knex.column('name','level','date_of_birth').select().from('teachers')
//     // let select4 = await knex.from('teachers').select().column('name','level','date_of_birth')
//     // console.log(select);
//     // console.log(select2);
//     // console.log(select3);
//     // console.log(select4)

//     // let teachers = await knex.select('name','level','date_of_birth').from('teachers')
//     //     .where('date_of_birth','>','1970-01-01')
//     //     .orWhere('level','=','degree')
//     //     // % - more than 1 char
//     //     // _ represents a single char
//     //     .andWhere('name','like','%e%')

//     // console.log(teachers);

//     // knex does not have a VALUE syntax
//     // multiple line using an array
//     // returning id to be assigned to variables
//     // let [jon, joffrey] = await knex('teachers').insert([
//     //     {name:'jon', level:'hd',date_of_birth:'1980-02-19'},
//     //     {name:'joffrey', level:'degree',date_of_birth:'1998-06-20'}
//     // ]).returning('id')

//     // console.log(jon);
//     // console.log(joffrey)
// }

// run();

// async function insertTeachers(){
//     const teachers = [{name: "cercei",level:'masters',date_of_birth:'1987-12-01'},
// {name:"jamie",level:'doctor',date_of_birth:'1986-03-14'}];

//     const batchSize = 1;
//     const ids = await knex.batchInsert("teachers",teachers,batchSize).returning('id');
//     console.log(ids);
// }
// insertTeachers();
