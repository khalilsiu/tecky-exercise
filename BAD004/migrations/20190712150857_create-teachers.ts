import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable('teachers');
    if (!hasTable){
        return knex.schema.createTable('teachers',(table)=>{
            table.increments();
            table.string('name');
            table.date("date_of_birth");
            table.string('level');
            table.timestamps(false, true);
        })
    }else{
        return Promise.resolve();
    }
    
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists('teachers')
}

