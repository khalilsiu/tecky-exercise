import * as Knex from "knex";


export async function up(knex: Knex) {
    const hasTable = await knex.schema.hasTable('teachers');
    if(hasTable){
        // to add columns, you just do table
        // up down action should be in pairs
        return knex.schema.alterTable('teachers',(table)=>{
            table.renameColumn('name','teacher_name');
            table.decimal('level',3).alter()
        })
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex) {
    const hasTable = await knex.schema.hasTable('teachers');
    if(hasTable){
        // to change the column, you need altertable
        return knex.schema.alterTable('teachers',(table)=>{
            table.renameColumn('teacher_name','name');
            table.string("level").alter();
        });
    }else{
        return Promise.resolve();
    }
}

