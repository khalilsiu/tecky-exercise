import * as Knex from 'knex';
import {StudentService} from './StudentService';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


const studentService = new StudentService(knex);

async function tryStudentService(){
    await studentService.addStudent({
        name: "new student",
        level : 5,
        date_of_birth:"1995-01-01"
    })
    const students = await studentService.getStudents();
    console.log(students);

}
tryStudentService();