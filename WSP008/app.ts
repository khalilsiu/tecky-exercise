import * as express from 'express';
import {DirectoryRouter} from './routers/DirectoryRouter'
import {FileRouter} from './routers/FileRouter';
import {UserRouter} from './routers/UserRouter';
import {FileService} from './services/FileService'


const app = express();

const directoryRouter = new DirectoryRouter();
const fileService = new FileService();
const fileRouter = new FileRouter(fileService);
const userRouter = new UserRouter()

app.use('/',userRouter.router());
app.use('/fs',userRouter.checkLogin,directoryRouter.router()); //undefined
app.use('/fs',userRouter.checkLogin,fileRouter.router());



const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});