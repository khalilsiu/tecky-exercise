import * as express from 'express';
import { Request, Response} from 'express';
import { listAllDir, listAllFile } from './listAllFile';
import * as fs from 'fs';
import * as pathLib from 'path';
import * as expressSession from 'express-session';


const app = express();
const ROOT = pathLib.join(__dirname, '..');
const USERS = fs.readFileSync('/Users/anthonysiu/codes/tecky-exercises/WSP008/report.json','utf-8');



app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));


// app.post('/login',(req,res))


app.post('/fs/register',async(req,res)=>{

    try{
        let {username, password} = req.query;
        if(!USERS){
            let users = [];
            users.push({
                "username":username,
                "password":password
            })
            // you need to JSON.stringify in order to not show [object,object] in JSON file
            await fs.promises.writeFile('/Users/anthonysiu/codes/tecky-exercises/WSP008/report.json', JSON.stringify(users),{flag:'w'});
                res.send('user registered');
        }
        else{
            let users: any[] = JSON.parse(USERS);
            console.log("before",users)
            for (let user of users){
                if (username === user.username){
                    throw new Error("Username taken")
                }
                
            }   
            users.push({
                "username":username,
                "password":password
            })
            console.log("after",users);
            // you need to JSON.stringify in order to not show [object,object] in JSON file
            await fs.promises.writeFile('/Users/anthonysiu/codes/tecky-exercises/WSP008/report.json', JSON.stringify(users),{flag:'w'});
            res.send('user registered');

        }

    }
    catch(e){
        res.send(e.message);
    }

})


app.get('/fs/directories', async (req: Request, res: Response) => {
    if (req.query.path) {
        let { path } = req.query;
        path = pathLib.join(ROOT, path);
        let dir = await listAllDir(path);
        res.json(dir);
    }
    else {
        let dir = await listAllDir(ROOT);
        res.json(dir);

    }
})


app.get('/fs/files', async (req: Request, res: Response) => {
    if (req.query.path) {
        let { path } = req.query;
        path = pathLib.join(ROOT, path);
        let files = await listAllFile(path);
        res.json(files);
    }
    else {
        let files = await listAllFile(ROOT);
        res.json(files);
    }
})

app.post('/fs/directory', async (req: Request, res: Response) => {
    let { path } = req.query;
    path = pathLib.join(ROOT, path);
    let dir = await listAllDir(ROOT);
    if (dir.includes(path)) {
        res.send("400")
        res.status(400);
    }
    else {
        fs.mkdir(path, function (err: Error) {
            if (err) {
                console.error(err.message);
            }
            else {
                res.send('done')
            }
        });
    }
})

app.get('/fs/file', async (req: Request, res: Response) => {
    console.log(req.query);
    let { path } = req.query;
    path = pathLib.join(ROOT, path);
    let files = await listAllFile(ROOT);
    console.log('path', path);
    console.log(files.includes(path));
    if (files.includes(path)) {
        res.sendFile(path);
    }
    else {
        res.send('404');
    }
})

app.delete('/fs/directory', async (req, res) => {
    let { path } = req.query;
    path = pathLib.join(ROOT, path);
    let dir = await listAllDir(ROOT);
    if (dir.includes(path)) {
        fs.rmdir(path, function (err: Error) {
            if (err) {
                console.error(err.message);
            }
            else {
                res.send('path deleted');
            }
        })
    }
    else {
        res.send('404')
    }

})

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}`);
})