


import * as fs from 'fs';
import * as path from 'path';



function fsReadFileNamePromise(filePath: string) {
    // when you define a class, you put <T> behind the class name Node<T>
    // when you new a class, you specify the type of the T: new Node<number> 
    return new Promise<string[]>(function (resolve, reject) {
        fs.readdir(filePath, function (err: Error, files: string[]) {
            if (err) {
                reject(err);
            }
            else {
                resolve(files);

            }


        })
    })
}

function fsStatPromise(filePath: string) {
    return new Promise<fs.Stats>(function (resolve, reject) {
        fs.stat(filePath, function (err: Error, stat: fs.Stats) {
            if (err) {
                reject(err)
                return;

            }
            else {
                resolve(stat);

            }
        })
    })
}



export async function listAllDir(filePath: string) {
    try {
        let list:string[] = [];
        const fileNames: string[] = await fsReadFileNamePromise(filePath);
        for (let fileName of fileNames) {
            let fullPathName: string = path.join(filePath, fileName);
            let stat = await fsStatPromise(fullPathName);
            if (stat.isDirectory()) {
                list.push(fullPathName);
            }
            
        }
        return (list);

    }
    catch (err) {
        return err;

    }
}

export async function listAllFile(filePath: string) {
    try {
        let list:string[] = [];
        const fileNames: string[] = await fsReadFileNamePromise(filePath);
        for (let fileName of fileNames) {
            let fullPathName: string = path.join(filePath, fileName);
            let stat = await fsStatPromise(fullPathName);
            if (stat.isFile()) {
                list.push(fullPathName);
            }
        }
        return (list);

    }
    catch (err) {
        return err;

    }
}



