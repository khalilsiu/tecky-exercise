import * as fs from 'fs';
import * as path from 'path';


export class FileService{
    private ROOT: string;
    constructor(){
        this.ROOT = path.join(__dirname, "..","..")
    }
    async listAllFile(filePath=this.ROOT) {
        try {
            if (filePath !== this.ROOT){
                filePath = path.join(this.ROOT, filePath);
            }
            let list:string[] = [];
            const fileNames: string[] = await fs.promises.readdir(filePath);
            console.log(fileNames)
            for (let fileName of fileNames) {
                let fullPathName: string = path.join(filePath, fileName);
                let stat = await fs.promises.stat(fullPathName);
                if (stat.isFile()) {
                    list.push(fullPathName);
                }
            }
            return list;
        }
        catch (err) {
            // return err;
            let list = [];
            list.push({"result":"files cannot be found"})
            return list;
        }
    }

    async toFullPath (filePath: any){
        try{
            // throw new Error();
            return filePath = path.join(this.ROOT, filePath);
        }
        catch(err){
            return {"result":"path cannot be converted to full"}
        }
    }

    async fileExists(filePath: any){
        try{
            filePath = await this.toFullPath(filePath);
            let fileList = await this.listAllFile(filePath);
            return (fileList.includes(filePath));
        }
        catch(err){
            return {"result":"file existence is unable to identified"}
        }
    }

    async downloadFile(filePath: any){
        try{
            if (this.fileExists(filePath)){
                return await this.toFullPath(filePath);
            }
            else{
                return {"result":"destination does not exist"};
            }
        }
        catch(err){
            return err;
        }
    }
}

let haha = new FileService();
console.log(path.join(__dirname,"test"))
haha.listAllFile(path.join(__dirname,"test")).then(x=>console.log(x))