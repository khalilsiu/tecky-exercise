import * as fs from 'fs';


export class UserService {
    private userRecords: { username: string, password: string }[];

    constructor() {
        if (!fs.readFileSync('./report.json', 'utf8')) {
            this.userRecords = [];
        }
        else {
            this.userRecords = JSON.parse(fs.readFileSync('./report.json', 'utf8'));

        }
        // console.log(this.userRecords);
    }

    async register(username: string, password: string) {
        try {

            if (this.userRecords.find(x => x.username === username)) {
                throw new Error();
            }
            else {
                this.userRecords.push({
                    "username": username,
                    "password": password
                })
                await fs.promises.writeFile('./report.json', JSON.stringify(this.userRecords), { flag: 'w' });
                return { "result": "user registered" };
            }
        }
        catch (err) {
            return { "result": "username is taken" };
        }

    }

    login(username:string, password: string){
        if (this.userRecords.find(x=>x.username === username) && this.userRecords.find(x=>x.password === password)){
            return true;
        }
        else{
            return false;
        }        
    }

}