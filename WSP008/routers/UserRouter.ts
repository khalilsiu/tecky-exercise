import * as express from 'express';
import {Request, Response, NextFunction} from 'express';
import {UserService} from '../services/UserService';
import * as bodyParser from 'body-parser';
import * as expressSession from 'express-session';

export class UserRouter{
    private userService: UserService;

    constructor(){
        this.userService = new UserService();
    }

    router(){
        const router = express.Router();
        router.use(bodyParser.urlencoded({extended:true}));
        router.use(bodyParser.json())
        router.use(expressSession({
            secret: 'File listing app',
            resave: true,
            saveUninitialized: true
        }))
        router.post('/login',this.login);
        router.post('/register', this.register);
        return router;
    }

    register = async(req:Request, res: Response) =>{
        try{
            // body does not work with GET method, change all query to body
            if (!req.body.username || !req.body.password){
                throw new Error(JSON.stringify({"result": "enter username or password"}));
            }
            else{
                let {username, password} = req.body;
                res.json(await this.userService.register(username,password));
            }
        }
        catch(err){
            res.status(400).send(err.message);
        }
    }

    login = async(req:Request, res:Response)=>{
        try{
            if (!req.body.username || !req.body.password){
                throw new Error(JSON.stringify({"result": "enter username or password"}));
            }
            else{
                let {username, password} = req.body;

                if (req.session && await (this.userService.login(username, password))){
                    req.session.name = username;
                    console.log(req.session.name);
                    res.status(200).json({"result":"you have logged in"})
                }
                else{
                    throw new Error(JSON.stringify({"result": "wrong password or username"}));
                }
                
            }
        }
        catch(err){
            res.status(401).send(err.message);
        }
    }

    checkLogin = (req:Request, res: Response, next: NextFunction)=>{
        try{
            if (req.session && req.session.name){
                next();
            }
            else{
                throw new Error();
            }
        }
        catch(err){
            res.status(401).json({"result":"you are not logged in"});
        }
    }


}