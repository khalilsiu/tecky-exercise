import * as express from 'express';
import {Request, Response} from 'express'
import {FileService} from '../services/FileService';

export class FileRouter{
    

    constructor(private fileService: FileService){
    }

    router(){
        const router = express.Router();
        router.get('/files', this.files);
        router.get('/file', this.file);
        return router;
    }

    files = async (req: Request,res: Response) =>{
        try{
            if (req.query.path){
                let {path} = req.query;
                console.log(path);
                res.json(await this.fileService.listAllFile(path));
            }
            else{
                res.json(await this.fileService.listAllFile());
            }
        }
        catch(err){
            res.status(400).send(err);
        }
    }
    file = async(req:Request,res:Response)=>{
        try{
            if(req.query.path){
                let {path} = req.query;
                // res.download only available for the files within the current directory
                // it only prints out
                res.download(await this.fileService.downloadFile(path));
            }

        }
        catch(err){
            return err;
        }
    }
}