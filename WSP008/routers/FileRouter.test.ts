import {FileService} from '../services/FileService'
import {FileRouter} from './FileRouter'
// import * as path from 'path'

type Mockify<T> = {
    [P in keyof T]: jest.Mock<{}>
}

describe("FileService",()=>{
    let router: FileRouter;
    let fileService : Mockify<FileService>;
    let req: any;
    let res: any;

    beforeEach(()=>{
        fileService = {
            listAllFile : jest.fn((filePath:string)=> ({})),
            toFullPath : jest.fn((filePath:any)=> ({})),
            fileExists : jest.fn((filePath:any)=> ({})),
            downloadFile : jest.fn((filePath:any)=> ({}))
        }
        router = new FileRouter(fileService as any as FileService);
        req = {
            query :{
                path:'hello'
            }
        }
        res = {
            json : jest.fn(()=>null),
            status: jest.fn(()=> {
                return {send: jest.fn(()=> null)}
            }),
            download: jest.fn(()=> null)
        }

    })
    it("res.json should have passed ['text.txt']",async()=>{
        (fileService.listAllFile as jest.Mock).mockReturnValue(['text.txt'])
        await router.files(req,res)
        expect(res.json).toHaveBeenCalledWith(['text.txt'])
    })
    it("res.json should have passed with ['text.txt'] with req.query.path as null",async ()=>{
        (fileService.listAllFile as jest.Mock).mockReturnValue(["text.txt"])
        req.query.path = null;
        await router.files(req,res);
        expect(res.json).toHaveBeenCalledWith(['text.txt'])
    })

    it("error in the listAllFile should return status 400",async ()=>{
        (fileService.listAllFile as jest.Mock).mockRejectedValue(Error('this is error'))
        await router.files(req,res);
        expect(res.status).toHaveBeenCalledWith(400)
    })

    it("res.download should be ", async ()=>{
        (fileService.downloadFile as jest.Mock).mockReturnValue(["test.txt"]);
        await router.file(req, res);
        expect(res.download).toHaveBeenCalledWith(["test.txt"])
    })
})