import * as express from 'express';
import {Request, Response} from 'express'
import {DirectoryService} from '../services/DirectoryService';

export class DirectoryRouter{
    private dirService: DirectoryService;

    constructor(){
        this.dirService = new DirectoryService();
    }

    router(){
        const router = express.Router();
        router.get('/directories', this.directories);
        router.post('/directory', this.directory);
        router.delete('/directory', this.directory);
        return router; // it has to return router, so that this function is able to produce another requestHandler to function, otherwise, will be undefined
    }

    directories = async (req: Request, res:Response)=>{
        try{
            if (req.query.path){
                // disruptive operator: it explodes req.query to use the path as a variable directly
                let {path} = req.query;
                res.json(await this.dirService.listAllDir(path));  
            }
            else{
                res.json(await this.dirService.listAllDir());
            }
        }
        catch(err){
            res.status(404).send(err);
        }
    }
    directory = async(req:Request, res:Response)=>{
        try{
            if (req.query.path){
                let {path} = req.query;
                if (await this.dirService.dirExist(path)){
                    res.json(await this.dirService.deleteDir(path));
                }
                else {
                    res.json(await this.dirService.createDir(path));
                }
            }
        }
        catch(err){
            res.status(400).send(err);
        }
    }
    
}